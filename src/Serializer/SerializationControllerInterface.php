<?php

namespace Drupal\spectrum\Serializer;

interface SerializationControllerInterface
{
  /**
   * Returns a serializer for the passed Entity and Bundle. In case no serializer is found, the application serializer is returned
   *
   * @param string $entity
   * @param string $bundle
   * @return SerializerInterface
   */
  public function getSerializer(string $entity, string $bundle): SerializerInterface;

  /**
   * Returns the default application serializer that will be used in case no custom serializer is defined for the passed Entity and Bundle
   *
   * @return SerializerInterface
   */
  public function getApplicationSerializer(): SerializerInterface;
}
