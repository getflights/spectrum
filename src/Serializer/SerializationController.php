<?php

namespace Drupal\spectrum\Serializer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\spectrum\Serializer\Annotation\Serializer;
use Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;

class SerializationController extends DefaultPluginManager implements LoggerAwareInterface, SerializationControllerInterface
{
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    private LoggerInterface $logger
  ) {
    parent::__construct('Plugin/Serializer', $namespaces, $moduleHandler, SerializerInterface::class, Serializer::class);
    $this->setCacheBackend($cacheBackend, 'serializer_plugins');
  }

  /**
   * @inheritDoc
   */
  public function setLogger(LoggerInterface $logger): void
  {
    $this->logger = $logger;
  }

  /**
   * @inheritDoc
   */
  public function getSerializer(string $entity, string $bundle): SerializerInterface
  {
    $foundPluginDefinitions = [];
    $wildCardDefinitions = [];

    foreach ($this->getDefinitions() as $pluginDefinition) {
      if ($pluginDefinition['entity'] === $entity && $pluginDefinition['bundle'] === $bundle) {
        $foundPluginDefinitions[] = $pluginDefinition;
      }
      if ($pluginDefinition['entity'] === $entity && $pluginDefinition['bundle'] === '*') {
        $wildCardDefinitions[] = $pluginDefinition;
      }
    }

    if (sizeof($foundPluginDefinitions) > 0) {
      return $this->getWeightedPluginDefinition($foundPluginDefinitions);
    }

    if (sizeof($wildCardDefinitions) > 0) {
      return $this->getWeightedPluginDefinition($wildCardDefinitions);
    }

    return $this->getApplicationSerializer();
  }

  private function getWeightedPluginDefinition(array $definitions): SerializerInterface
  {
    if (sizeof($definitions) === 0) {
      throw new RuntimeException('No definitions passed');
    }

    uasort($definitions, function (array $definition1, array $definition2) {
      return $definition2['weight'] <=> $definition1['weight'];
    });

    /** @var SerializerInterface */
    return $this->createInstance($definitions[0]['id']);
  }

  /**
   * {@inheritdoc}
   */
  public function getApplicationSerializer(): SerializerInterface
  {
    $definitions = $this->getDefinitions();
    $foundPluginDefinitions = [];
    foreach ($this->getDefinitions() as $pluginDefinition) {
      if ($pluginDefinition['entity'] === '*' && $pluginDefinition['bundle'] === '*') {
        $foundPluginDefinitions[] = $pluginDefinition;
      }
    }

    if (sizeof($foundPluginDefinitions) > 0) {
      return $this->getWeightedPluginDefinition($foundPluginDefinitions);
    }

    $this->logger->critical('No application serializer found, please register a Plugin with entity = * and bundle = *');
    throw new Exception('No application serializer found, please register a Plugin with entity = * and bundle = *');
  }
}
