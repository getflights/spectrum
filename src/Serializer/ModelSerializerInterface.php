<?php

namespace Drupal\spectrum\Serializer;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\spectrum\Model\CollectionInterface;
use Drupal\spectrum\Model\ModelInterface;
use Getflights\Jsonapi\Serializer\JsonApiDataNode;
use Getflights\Jsonapi\Serializer\JsonApiNode;
use stdClass;

interface ModelSerializerInterface {

  public const SERIALIZE_AS_FIELD = 'field';

  public const SERIALIZE_AS_RELATIONSHIP = 'relationship';

  /**
   * Returns a array of defaults fields on a drupal entity that should be
   * ignored in serialization
   *
   * @return string[]
   */
  public function getDefaultIgnoreFields(): array;

  /**
   * @param string $modelClass
   *
   * @return array
   */
  public function getPrettyFieldsToFieldsMapping(string $modelClass): array;

  /**
   * @param string $modelClass
   *
   * @return array
   */
  public function getFieldsToPrettyFieldsMapping(string $modelClass): array;

  /**
   * @param string $modelClass
   * @param string $field
   *
   * @return string|null
   */
  public function getFieldForPrettyField(string $modelClass, string $field): ?string;

  /**
   * @param string $modelClass
   * @param string $field
   *
   * @return string|null
   */
  public function getPrettyFieldForField(string $modelClass, string $field): ?string;

  /**
   * @param string $modelClass
   * @param string $field
   *
   * @return boolean
   */
  public function prettyFieldExists(string $modelClass, string $field): bool;

  /**
   * Get the Pretty Field for an Underscored Field (for example translates
   * first_name to first-name)
   *
   * @param string $underscoredField
   *
   * @return string
   */
  public function getPrettyFieldForUnderscoredField(string $modelClass, string $underscoredField): string;

  /**
   * Checks whether an underscored field exists on this model, this is to
   * correctly translate a field like first_name back to field_first_name
   *
   * @param string $underscoredField
   *
   * @return boolean
   */
  public function underScoredFieldExists(string $modelClass, string $underscoredField): bool;

  /**
   * Return a jsonapi.org compliant Serialization type (will dasherize types),
   * normally the drupal entity type/bundle will be used, but it is possible to
   * set an alias at runtime See setSerializationTypeAlias()
   *
   * @param string $modelClass
   *
   * @return string
   */
  public function getSerializationType(string $modelClass): string;

  /**
   * This hacky method sets a different serialization type at runtime than the
   * Drupal type. (it is used to give an Entity a different name in a different
   * scenario upon serialization and deserialization)
   *
   * @param string $modelClass
   * @param string $type
   *
   * @return void
   */
  public function setSerializationTypeAlias(string $modelClass, string $type): void;

  /**
   * This function will return a JsonApiNode representation of the current
   * model. Necessary checks will be done to make sure the user has access to
   * the fields he wants to serialize. If the user doesnt have access, fields
   * will omitted from the JsonApiNode
   *
   * @param ModelInterface $model
   *
   * @return JsonApiNode
   */
  public function getJsonApiNodeForModel(ModelInterface $model): JsonApiNode;

  /**
   * Converts the Collection to a JsonApiDataNode
   *
   * @return JsonApiDataNode
   */
  public function getJsonApiNodeForCollection(CollectionInterface $collection): JsonApiDataNode;

  /**
   * Returns a serialized JsonApiRootNode
   *
   * @param ModelInterface $model
   *
   * @return \stdClass
   */
  public function serializeModel(ModelInterface $model): stdClass;

  /**
   * Serializes the collection to a jsonapi.org compliant stdClass
   *
   * @param CollectionInterface $collection
   *
   * @return \stdClass
   */
  public function serializeCollection(CollectionInterface $collection): stdClass;

  /**
   * This function will update the values of the entity based on the values of
   * a jsonapi.org compliant object based on the field type, necessary
   * transforms to the drupal datastructure will be done. Necessary checks will
   * be done to make sure the user has permission to edit the field. In case no
   * permission is granted, the field on the entity will not be updated
   *
   * @param ModelInterface $model
   * @param \stdClass $deserialized jsonapi.org document
   */
  public function deserializeJsonApiIntoModel(ModelInterface $model, stdClass $jsonApiDocument): void;

  /**
   * Returns the serialized value for a single field or relationship
   * In case the passed field is a entity_reference or
   * entity_reference_revisions an JsonApiNode is returned, else the value can
   * be anything
   *
   * @param ModelInterface $model
   * @param string $fieldName
   *
   * @return mixed
   */
  public function getSerializedValueForField(ModelInterface $model, string $fieldName): mixed;

  /**
   * Returns either "field" or "relationship" depending on how to
   * serialize/deserialize the passed fieldDefinition see class constants
   *
   * @param FieldDefinitionInterface $fieldDefinition
   *
   * @return string
   */
  public function getSerializeAs(FieldDefinitionInterface $fieldDefinition): string;

  public function getSerializationModel(string $type): string;

}
