<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class JobTerminateException extends Exception
{ }
