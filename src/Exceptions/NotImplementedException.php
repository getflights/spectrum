<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class NotImplementedException extends Exception
{ }
