<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class RelationshipNotDefinedException extends Exception
{ }
