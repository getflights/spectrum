<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class InvalidDirectionException extends Exception
{ }
