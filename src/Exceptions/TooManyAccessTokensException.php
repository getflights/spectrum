<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class TooManyAccessTokensException extends Exception
{ }
