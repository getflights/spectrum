<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class InvalidFieldException extends Exception
{ }
