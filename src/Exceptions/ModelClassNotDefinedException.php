<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class ModelClassNotDefinedException extends Exception
{ }
