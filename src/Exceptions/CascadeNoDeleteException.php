<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class CascadeNoDeleteException extends Exception
{ }
