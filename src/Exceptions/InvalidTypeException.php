<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class InvalidTypeException extends Exception
{ }
