<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class InvalidEntityException extends Exception
{ }
