<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class ModelNotPersistedException extends Exception
{ }
