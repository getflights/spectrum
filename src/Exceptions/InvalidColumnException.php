<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class InvalidColumnException extends Exception
{ }
