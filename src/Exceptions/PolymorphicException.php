<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class PolymorphicException extends Exception
{ }
