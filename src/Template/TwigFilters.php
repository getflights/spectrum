<?php

namespace Drupal\spectrum\Template;

use CommerceGuys\Addressing\Address;
use Drupal\spectrum\Model\SimpleCollectionWrapper;
use Drupal\spectrum\Model\SimpleModelWrapper;
use Drupal\spectrum\Models\File;
use Drupal\spectrum\Models\Image;
use Drupal\spectrum\Services\ModelServiceInterface;
use Drupal\spectrum\Utils\AddressUtils;
use Drupal\spectrum\Utils\DateUtils;
use Drupal\spectrum\Utils\LanguageUtils;
use Drupal\spectrum\Utils\NumberUtils;
use Drupal\spectrum\Utils\StringUtils;
use Money\Money;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * THis class contains a whole list of Twig Filters to make it easier to use model functionality in twig renders
 */
class TwigFilters extends AbstractExtension
{
  /**
   * Generates a list of all Twig filters that this extension defines.
   *
   * @return array
   */
  public function getFilters()
  {
    return [
      new TwigFilter('src', [$this, 'src']),
      new TwigFilter('file_src', [$this, 'fileSrc']),
      new TwigFilter('base64src', [$this, 'base64src']),
      new TwigFilter('address_format', [$this, 'addressFormat']),
      new TwigFilter('bundle_key', [$this, 'bundleKey']),
      new TwigFilter('price', [$this, 'price']),
      new TwigFilter('autonumber_format', [$this, 'autonumberFormat']),
      new TwigFilter('pad_left', [$this, 'padLeft']),
      new TwigFilter('pnr_safe_name', [$this, 'pnrSafeName']),
      new TwigFilter('target_id', [$this, 'targetId']),
      new TwigFilter('collection_sort', [$this, 'collectionSort']),
      new TwigFilter('tt', [$this, 'translate']),
      new TwigFilter('mask', [$this, 'mask']),
      new TwigFilter('index_number', [$this, 'indexNumber'])
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   * @return string
   */
  public function getName()
  {
    return 'spectrum.twigfilters';
  }

  /**
   * Get the image SRC with possible image style
   *
   * @param Image $image
   * @param string $imagestyle
   * @return string
   */
  public static function src(Image $image, string $imagestyle = null): string
  {
    return $image->getSRC($imagestyle);
  }

  /**
   * Returns the SRC of the passed in File
   *
   * @param File $file
   * @return string
   */
  public static function fileSrc(File $file): string
  {
    return $file->getSRC();
  }


  /**
   * Get the base 64 image SRC with possible image style
   *
   * @param Image $image
   * @param string $imagestyle
   * @return string
   */
  public static function base64src(Image $image, string $imagestyle = null): string
  {
    return $image->getBase64SRC($imagestyle);
  }

  /**
   * Formats an address according to the country of the address formatting policy
   *
   * @param Address|null $address
   * @return string|null
   */
  public static function addressFormat(?Address $address, ?string $originCountry = null): ?string
  {
    return AddressUtils::format($address, $originCountry);
  }

  /**
   * @param SimpleModelWrapper $model
   * @return string
   */
  public static function bundleKey($model): string
  {
    if ($model instanceof SimpleModelWrapper) {
      $model = $model->getModel();

      /** @var ModelServiceInterface $modelService */
      $modelService = \Drupal::service('spectrum.model');

      return $modelService->getBundleKey(get_class($model));
    }

    return '';
  }

  /**
   * Returns a formatted price with Currency
   *
   * @param number|string|Money $price
   * @param string $currency
   * @return string
   */
  public static function price($price, string $currency = null): string
  {
    if (!is_a($price, Money::class)) {
      return number_format((float) $price, 2, ',', ' ') . ' ' . $currency;
    }

    /** @var Money $price */
    return number_format((float) NumberUtils::getDecimal($price), 2, ',', ' ') . ' ' . $price->getCurrency();
  }

  /**
   * Apply str_pad to the provided value
   *
   * @param string $value
   * @param integer $length
   * @param string $padvalue
   * @return string
   */
  public static function padLeft(string $value, int $length, string $padvalue = '0'): string
  {
    return str_pad($value, $length, $padvalue, STR_PAD_LEFT);
  }

  /**
   * Apply an AutoNumber format to the passed in value
   *
   * @param string $value
   * @param string $format
   * @param \DateTime $date The date you want to use for the dynamic part of the autonumber format
   * @return string
   */
  public static function autonumberFormat(string $value, string $format, \DateTime $date): string
  {
    $formatted = DateUtils::generatePatternString($date, $format);
    $formatted =  str_replace('{{VALUE}}', $value, $formatted);
    return $formatted;
  }

  /**
   * Strip the value of all invalid characters so it can be used in a PNR
   *
   * @param string $value
   * @return string
   */
  public static function pnrSafeName(string $value): string
  {
    return StringUtils::pnrSafeString($value);
  }

  /**
   * Returns the target_id for a specific field
   *
   * @param SimpleModelWrapper $simpleModel
   * @param string $field
   * @return int
   */
  public static function targetId(SimpleModelWrapper $simpleModel, string $field)
  {
    return $simpleModel->getModel()->entity->$field->target_id;
  }

  /**
   * Sort the provided collection through a passed in sorting function name (which should be available on the model)
   *
   * @param SimpleCollectionWrapper $simpleCollection
   * @param string $sortingFunction
   * @return SimpleCollectionWrapper
   */
  public static function collectionSort(SimpleCollectionWrapper $simpleCollection, string $sortingFunction): SimpleCollectionWrapper
  {
    $collection = $simpleCollection->getCollection();
    $collection->sort($sortingFunction);

    return $simpleCollection;
  }

  /**
   * Translate the passed in value through the Drupal t function, extra varialbes can be passed
   *
   * @param string $value
   * @param array $variables
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public static function translate(string $value, array $variables = [])
  {
    return LanguageUtils::translate($value, $variables);
  }

  /**
   * Generates a masked string for the provided value
   *
   * @param string $value The value you want to mask
   * @param string $mask The mask to use, use {{1}} {{2}} ... To represent capturing groups from regex
   * @param string $regex The regex (with capturing groups) to use for the mask
   * @return string
   */
  public static function mask(?string $value, string $mask, string $regex): string
  {
    if (empty($value)) {
      return '';
    }

    //value: 190000010188
    //regex: #^(d{3})(d{4})(d{5})$#
    //regex: #^(\d{3})(\d{4})(\d{5})$#
    preg_match($regex, $value, $values);
    preg_match_all('#\{\{(\d+)\}\}#', $mask, $replaces);

    $returnValue = $mask;
    foreach ($replaces[0] as $index => $replaceValue) {
      $returnValue = str_replace($replaceValue, $values[$replaces[1][$index]], $returnValue);
    }

    return $returnValue;
  }

  /**
   * Format indexnumber for giftcard
   * @param string $value
   * @return string
   * @throws \Exception
   */
  public static function indexNumber(string $value)
  {
    $strlength = strlen($value);
    $date = new \DateTime('now', new \DateTimeZone('UTC'));
    $formatted = $date->format('y');

    return str_pad($formatted, 6  - $strlength, '0') . $value;
  }
}
