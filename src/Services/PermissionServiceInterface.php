<?php

namespace Drupal\spectrum\Services;

use Drupal\Core\Session\AccountInterface;
use Drupal\mist_platform\Api\ApiHandlerInterface;
use Drupal\spectrum\Models\User;
use Fig\Http\Message\RequestMethodInterface;
use http\Env\Request;
use Psr\Http\Message\RequestInterface;

/**
 * This interface exposes the different functions Spectrum needs to be able to provide proper access or security checks.
 * Everyone can choose to implement this how they want, as long as the service is registered in the container, and these functions are present
 */
interface PermissionServiceInterface
{
  public const ACCESS_CREATE = 'C';
  public const ACCESS_READ = 'R';
  public const ACCESS_UPDATE = 'U';
  public const ACCESS_DELETE = 'D';

  /**
   * Trigger this when you want to remove a user from the access policies completely
   * This will be triggered automatically after a delete of a user.
   *
   * @param User $user
   * @return void
   */
  public function removeUserFromAccessPolicies(User $user): void;

  /**
   * Trigger this when the access policy of a user needs to be recalculated.
   * This will be automatically triggered after an insert, and update of a User
   * @param User $user
   *
   * @throws \Exception
   */
  public function rebuildAccessPoliciesForUser(User $user): void;

  /**
   * Rebuilds the access policy table.
   */
  public function rebuildAccessPolicy(): void;

  /**
   * Rebuilds the access policy for a specific entity
   *
   * @param string $entity
   * @return void
   */
  public function rebuildAccessPolicyForEntity(string $entity): void;

  /**
   * Rebuilds the access policy for a specific entity and bundle
   *
   * @param string $entity
   * @param string $bundle
   * @return void
   */
  public function rebuildAccessPolicyForEntityAndBundle(string $entity, string $bundle): void;

  /**
   * Rebuilds the access policy for a specific model class
   *
   * @param string $class
   * @return void
   */
  public function rebuildAccessPolicyForModelClass(string $class): void;

  /**
   * This function checks whether the given role has a certain access to the provided entity
   *
   * @param string $role The Drupal role of the User (for example, administrator, authenticated, anonymous, or any custom role)
   * @param string $permission The permission that will be checked in the form of "entity_bundle" (for example node_article)
   * @param string $access The access that will be checked C, R, U or D (Create, Read, Update or Delete)
   * @return boolean
   */
  public function roleHasModelPermission(string $role, string $permission, string $access): bool;

  /**
   * This function checks whether a Permission is defined for the API
   *
   * @param string $route The Symfony route of your BaseApiController (for example "spectrum.content")
   * @param string $api The API key in your controller that redirects to your BaseApiHandler (for example "nodes")
   * @return boolean
   */
  public function apiPermissionExists(string $route, string $api): bool;

  /**
   * This function checks whether an action Permission is defined for the API
   *
   * @param string $route The Symfony route of your BaseApiController (for example "spectrum.content")
   * @param string $api The API key in your controller that redirects to your BaseApiHandler (for example "nodes")
   * @return boolean
   */
  public function apiActionPermissionExists(string $route, string $api): bool;


  /**
   * This function checks whether a scope has access to a certain API
   *
   * @param string $scope
   * @param string $service
   * @param string $api
   * @return boolean
   */
  public function scopeHasApiPermission(string $scope, string $service, string $api): bool;

  /**
   * Check whether the provided user role has the correct API Permission
   *
   * @param string $role The Drupal role of the User (for example, administrator, authenticated, anonymous, or any custom role)
   * @param string $route The Symfony route of your BaseApiController (for example "spectrum.content")
   * @param string $api The API key in your controller that redirects to your BaseApiHandler (for example "nodes")
   * @param string $access The access that will be checked C, R, U or D (Create, Read, Update or Delete)
   * @return boolean
   */
  public function roleHasApiPermission(string $role, string $route, string $api, string $access): bool;

  /**
   * This function checks whether an API is publicly accessible 
   * (does not require a login, and is thus accessible by the AnonymousUserSession)
   *
   * @param string $route
   * @param string $api
   * @return boolean
   */
  public function apiIsPubliclyAccessible(string $route, string $api, ?string $action): bool;

  /**
   * Check whether a Drupal Role, has a certain OAuth scope
   *
   * @param string $role
   * @param string $scope
   * @return boolean
   */
  public function roleHasOAuthScopePermission(string $role, string $scope): bool;

  /**
   * Check whether a Drupal Role, has access to an APIHandler Action function
   *
   * @param string $role
   * @param string $route
   * @param string $api
   * @param string $action
   * @return boolean
   */
  public function roleHasApiActionPermission(string $role, string $route, string $api, string $action): bool;

  /**
   * Check whether a provided Drupal Role has access to a certain field
   *
   * @param string $role
   * @param string $entity
   * @param string $field
   * @param string $access Can either be 'view' (when a user just wants to READ a field) or 'edit' (when a user wants to change the value of a field)
   * @return boolean
   */
  public function roleHasFieldPermission(string $role, string $entity, string $field, string $access): bool;

  /**
   * Checks whether a User has an OAuth Scope permission
   *
   * @param User $user
   * @param string $scope
   * @return boolean
   */
  public function userHasOAuthScopePermission(User $user, string $scope): bool;

  /**
   * Check if a User has a Model Permission
   *
   * @param User $user
   * @param string $modelClass
   * @param string $access
   * @return boolean
   */
  public function userHasModelPermission(User $user, string $modelClass, string $access): bool;

  /**
   * Checks whether a user has access to a certain field on a Model
   * 
   * @param User $user
   * @param string $modelClass The Fully qualified classname of the model
   * @param string $field The field on the model (for example "field_body")
   * @param string $access What type of access ("view" or "edit")
   * @return boolean
   */
  public function userHasFieldPermission(User $user, string $modelClass, string $field, string $access): bool;

  /**
   * Checks if a user has access to view a field
   *
   * @param User $user
   * @param string $modelClass The Fully qualified classname of the model
   * @param string $field The field on the model (for example "field_body")
   * @return boolean
   */
  public function userHasFieldViewPermission(User $user, string $modelClass, string $field): bool;

  /**
   * Checks if a user has access to edit a field
   *
   * @param User $user
   * @param string $modelClass The Fully qualified classname of the model
   * @param string $field The field on the model (for example "field_body")
   * @return boolean
   */
  public function userHasFieldEditPermission(User $user, string $modelClass, string $field): bool;

  /**
   * Checks whether the logged in user has access to a certain field on a Model
   * 
   * @param string $modelClass The Fully qualified classname of the model
   * @param string $field The field on the model (for example "field_body")
   * @param string $access What type of access ("view" or "edit")
   * @return boolean
   */
  public function currentUserHasFieldPermission(string $modelClass, string $field, string $access): bool;

  /**
   * Checks whether the logged in user has view access to a certain field on a model
   *
   * @param string $modelClass The Fully qualified classname of the model
   * @param string $field The field on the model (for example "field_body")
   * @return boolean
   */
  public function currentUserHasFieldViewPermission(string $modelClass, string $field): bool;

  /**
   * Checks whether the logged in user has edit access to a certain field on a model
   *
   * @param string $modelClass The Fully qualified classname of the model
   * @param string $field The field on the model (for example "field_body")
   * @return boolean
   */
  public function currentUserHasFieldEditPermission(string $modelClass, string $field): bool;

  /**
   * Check if the current user has a Model Permission
   *
   * @param User $user
   * @param string $modelClass
   * @param string $access
   * @return boolean
   */
  public function currentUserHasModelPermission(string $modelClass, string $access): bool;

  public function userHasAccessToMethod(
    AccountInterface $user,
    ApiHandlerInterface $apiHandler,
    string $service,
    string $api,
    string $method,
  ): bool;

  public function userHasAccessToAction(
    AccountInterface $user,
    ApiHandlerInterface $apiHandler,
    string $service,
    string $api,
    string $action
  ): bool;
}
