<?php

namespace Drupal\spectrum\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\mist_platform\Api\Annotation\Action;
use Drupal\mist_platform\Api\ApiHandlerInterface;
use Drupal\spectrum\Model\Model;
use Drupal\spectrum\Model\ModelInterface;
use Drupal\spectrum\Models\User;
use Drupal\spectrum\Permissions\AccessPolicy\AccessPolicyEntity;
use Drupal\spectrum\Permissions\AccessPolicy\AccessPolicyInterface;
use Psr\Log\LoggerInterface;
use ReflectionAttribute;
use ReflectionObject;
use Symfony\Component\HttpFoundation\Request;

class PermissionService implements PermissionServiceInterface
{
  public function __construct(
    private LoggerInterface $logger,
    private ModelServiceInterface $modelService,
    private Connection $database,
    private CurrentUserServiceInterface $currentUser,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function roleHasOAuthScopePermission(string $role, string $scope): bool
  {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function roleHasModelPermission(string $role, string $permission, string $access): bool
  {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function roleHasApiPermission(string $role, string $route, string $api, string $access): bool
  {
    return FALSE;
  }

  public function userHasAccessToMethod(
    AccountInterface $user,
    ApiHandlerInterface $apiHandler,
    string $service,
    string $api,
    string $method
  ): bool {
    $accessString = $this->getAccessStringForMethod($method);

    foreach ($this->currentUser->get()->getRoles() as $role) {
      if ($this->roleHasApiPermission($role, $service, $api, $accessString)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  public function userHasAccessToAction(
    AccountInterface $user,
    ApiHandlerInterface $apiHandler,
    string $service,
    string $api,
    string $action
  ): bool {
    $method = 'action' . ucfirst($action);

    $reflectionObject = new ReflectionObject($apiHandler);
    $reflectionMethod = $reflectionObject->getMethod($method);
    $attribute = $reflectionMethod->getAttributes(Action::class)[0] ?? NULL;

    if ($attribute !== NULL) {
      /** @var ReflectionAttribute $action */
      /** @noinspection CallableParameterUseCaseInTypeContextInspection */
      $action = $attribute->newInstance();

      /** @var \Drupal\mist_platform\Api\Annotation\Action $action */
      return $user->hasPermission($action->permission);
    }

    // Fallback to the old system.
    foreach ($user->getRoles() as $role) {
      if ($this->roleHasApiActionPermission($role, $service, $api, $action)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  private function getAccessStringForMethod(string $method): string
  {
    return match ($method) {
      Request::METHOD_GET, Request::METHOD_HEAD => 'R',
      Request::METHOD_POST => 'C',
      Request::METHOD_PATCH, Request::METHOD_PUT => 'U',
      Request::METHOD_DELETE => 'D',
    };
  }

  /**
   * {@inheritdoc}
   */
  public function roleHasApiActionPermission(string $role, string $route, string $api, string $action): bool
  {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function apiPermissionExists(string $route, string $api): bool
  {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function scopeHasApiPermission(string $scope, string $service, string $api): bool
  {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function apiIsPubliclyAccessible(string $route, string $api, ?string $action): bool
  {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function apiActionPermissionExists(string $route, string $api): bool
  {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function roleHasFieldPermission(string $role, string $entity, string $field, string $access): bool
  {
    return FALSE;
  }

  /**
   * Rebuilds the access policy table.
   *
   * @todo move this upstream and use dependency injection to override this
   * class.
   */
  public function rebuildAccessPolicy(): void
  {
    $classes = $this->modelService->getRegisteredModelClasses();

    foreach ($classes as $class) {
      $this->rebuildAccessPolicyForModelClass($class);
    }
  }

  /**
   * Rebuilds the access policy for a specific entity
   *
   * @param string $entity
   *
   * @return void
   */
  public function rebuildAccessPolicyForEntity(string $entity): void
  {
    $classes = $this->modelService->getRegisteredModelClasses();

    /** @var ModelInterface $class */
    foreach ($classes as $class) {
      if ($class::entityType() === $entity) {
        /** @var string $class */
        $this->rebuildAccessPolicyForModelClass($class);
      }
    }
  }

  /**
   * Rebuilds the access policy for a specific entity and bundle
   *
   * @param string $entity
   * @param string $bundle
   *
   * @return void
   */
  public function rebuildAccessPolicyForEntityAndBundle(string $entity, string $bundle): void
  {
    $classes = $this->modelService->getRegisteredModelClasses();

    /** @var ModelInterface $class */
    foreach ($classes as $class) {
      if ($class::entityType() === $entity && $class::bundle() === $bundle) {
        /** @var string $class */
        $this->rebuildAccessPolicyForModelClass($class);
      }
    }
  }

  /**
   * Rebuilds the access policy for a specific model class
   *
   * @param string|Model $class
   *
   * @return void
   */
  public function rebuildAccessPolicyForModelClass(string $class): void
  {
    /** @var ModelInterface $class */
    $accessPolicy = $class::getAccessPolicy();

    /** @var string $class */
    $accessPolicy->rebuildForModelClass($class);
  }

  /**
   * @param int $uid
   */
  public function removeUserFromAccessPolicies(User $user): void
  {
    $this->database
      ->delete(AccessPolicyInterface::TABLE_ENTITY_ACCESS)
      ->condition('value', 'user:' . $user->getId())
      ->execute();
  }

  /**
   * @param int $uid
   *
   * @throws \Exception
   */
  public function rebuildAccessPoliciesForUser(User $user): void
  {
    $this->removeUserFromAccessPolicies($user);

    /** @var AccessPolicyEntity[] $values */
    $values = [];

    $classes = $this->modelService->getRegisteredModelClasses();
    /** @var Model $class */
    foreach ($classes as $class) {
      $accessPolicy = $class::getAccessPolicy();

      /** @var string $class */
      /** @var AccessPolicyEntity[] $values */
      $values = array_merge($values, $accessPolicy->getUserAccessForModelClass($user, $class));
    }

    if (!empty($values)) {
      $insertQuery = $this->database
        ->insert(AccessPolicyInterface::TABLE_ENTITY_ACCESS)
        ->fields(['entity_type', 'entity_id', 'value']);

      $chunks = array_chunk($values, 10000);
      foreach ($chunks as $chunk) {
        foreach ($chunk as $value) {
          $insertQuery = $insertQuery->values($value->getInsertValue());
        }
      }

      $insertQuery->execute();
    }
  }

  /**
   * Returns the base permission key in the form of "entity_bundle" (for example node_article) this is used for the
   * permission checker
   *
   * @param string $modelClass
   *
   * @return string
   */
  public function getPermissionKeyForModelClass(string $modelClass): string
  {
    return str_replace('.', '_', $this->modelService->getModelClassKey($modelClass));
  }

  /**
   * {@inheritdoc}
   */
  public function userHasFieldPermission(User $user, string $modelClass, string $field, string $access): bool
  {
    $permissionKey = $this->getPermissionKeyForModelClass($modelClass);

    $allowed = FALSE;
    foreach ($user->getRoles() as $role) {
      if ($this->roleHasFieldPermission($role, $permissionKey, $field, $access)) {
        $allowed = TRUE;
        break;
      }
    }

    return $allowed;
  }

  /**
   * {@inheritdoc}
   */
  public function userHasOAuthScopePermission(User $user, string $scope): bool
  {
    $allowed = FALSE;
    foreach ($user->getRoles() as $role) {
      if ($this->roleHasOAuthScopePermission($role, $scope)) {
        $allowed = TRUE;
        break;
      }
    }

    return $allowed;
  }

  /**
   * {@inheritdoc}
   */
  public function userHasModelPermission(User $user, string $modelClass, string $access): bool
  {
    $allowed = FALSE;
    $permission = $this->getModelClassPermissionKey($modelClass);
    foreach ($user->getRoles() as $role) {
      if ($this->roleHasModelPermission($role, $permission, $access)) {
        $allowed = TRUE;
        break;
      }
    }

    return $allowed;
  }

  /**
   * {@inheritdoc}
   */
  public function userHasFieldViewPermission(User $user, string $modelClass, string $field): bool
  {
    return $this->userHasFieldPermission($user, $modelClass, $field, 'view');
  }

  /**
   * {@inheritdoc}
   */
  public function userHasFieldEditPermission(User $user, string $modelClass, string $field): bool
  {
    return $this->userHasFieldPermission($user, $modelClass, $field, 'edit');
  }

  /**
   * {@inheritdoc}
   */
  public function currentUserHasFieldPermission(string $modelClass, string $field, string $access): bool
  {
    return $this->userHasFieldPermission($this->currentUser->get(), $modelClass, $field, $access);
  }

  /**
   * {@inheritdoc}
   */
  public function currentUserHasFieldViewPermission(string $modelClass, string $field): bool
  {
    return $this->userHasFieldPermission($this->currentUser->get(), $modelClass, $field, 'view');
  }

  /**
   * {@inheritdoc}
   */
  public function currentUserHasFieldEditPermission(string $modelClass, string $field): bool
  {
    return $this->userHasFieldPermission($this->currentUser->get(), $modelClass, $field, 'edit');
  }

  /**
   * {@inheritdoc}
   */
  public function currentUserHasModelPermission(string $modelClass, string $access): bool
  {
    return $this->userHasModelPermission($this->currentUser->get(), $modelClass, $access);
  }

  /**
   * This function will generate a key for a fully qualified classname so this can be used as a readable key in the
   * permissions table, instead of having to add FQCN
   *
   * @param string $modelClass
   *
   * @return string
   */
  private function getModelClassPermissionKey(string $modelClass): string
  {
    /** @var ModelInterface $modelClass */
    return str_replace('.', '_', $this->modelService->getKeyForEntityAndBundle($modelClass::entityType(), $modelClass::bundle()));
  }
}
