<?php

namespace Drupal\spectrum\Services;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\spectrum\Models\User;

interface CurrentUserServiceInterface
{
  /**
   * Returns the Current User as a spectrum Model
   *
   * @return User
   */
  public function get(): User;

  public function getAccountProxy(): AccountProxyInterface;

}
