<?php

namespace Drupal\spectrum\Services;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\Bytes;
use Drupal\Component\Utility\Environment;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\Utility\Token;
use Drupal\field\FieldConfigInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\spectrum\Exceptions\NotImplementedException;
use Drupal\spectrum\Services\ModelServiceInterface;
use Drupal\spectrum\Models\File;
use Drupal\spectrum\Services\PermissionServiceInterface;
use Drupal\spectrum\Serializer\ModelSerializerInterface;
use Drupal\spectrum\Utils\StringUtils;
use Exception;
use Getflights\Jsonapi\Serializer\JsonApiErrorNode;
use Getflights\Jsonapi\Serializer\JsonApiErrorRootNode;
use Getflights\Jsonapi\Serializer\JsonApiRootNode;
use Getflights\Jsonapi\Serializer\JsonApiSerializableNodeInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class FileService implements FileServiceInterface
{
  public function __construct(
    private LoggerInterface $logger,
    private ModelServiceInterface $modelService,
    private PermissionServiceInterface $permissionService,
    private ModelSerializerInterface $modelSerializer,
    private Token $tokenService,
    private FileSystemInterface $fileSystem,
    private StreamWrapperManagerInterface $streamWrapperManager,
    private FileRepositoryInterface $fileRepository
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getTargetFromRequest(Request $request): ?string
  {
    return $request->headers->get('X-Mist-Field-Target');
  }

  /**
   * {@inheritdoc}
   */
  public function createNewFile(string $uriScheme, string $directory, string $filename, $data): File
  {
    $directory = trim(trim($directory), '/');
    // Replace tokens. As the tokens might contain HTML we convert it to plaintext.
    $directory = PlainTextOutput::renderFromHtml($this->tokenService->replace($directory, []));
    $filename = basename($filename);

    // We build the URI
    $target = $uriScheme . '://' . $directory;

    // Prepare the destination directory.
    if ($this->fileSystem->prepareDirectory($target, FileSystemInterface::CREATE_DIRECTORY)) {
      // The destination is already a directory, so append the source basename.
      $target = $this->streamWrapperManager->normalizeUri($target . '/' . $this->fileSystem->basename($filename));

      // Create or rename the destination
      $this->fileSystem->getDestinationFilename($target, FileExists::Rename);

      // Save the blob in a File Entity
      $fileEntity = $this->fileRepository->writeData($data, $target);
      $file = new File($fileEntity); // TODO File/Image model fix
      // we want the file to disappear when it is not attached to a record
      // we put the status on 0, if it is attached somewhere, Drupal will make sure it is not deleted
      // When the attached record is deleted, the corresponding file will follow suit as well.
      // 6 hours after last modified date for a file, and not attached to a record, cron will clean up the file
      $file->entity->{'status'}->value = 0;
      $file->save();

      return $file;
    }

    // Perhaps $destination is a dir/file?
    $dirname = $this->fileSystem->dirname($target);
    if (!$this->fileSystem->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY)) {
      throw new Exception('File could not be moved/copied because the destination directory ' . $target . ' is not configured correctly.');
    }

    throw new NotImplementedException('Functionality not implemented');
  }

  /**
   * {@inheritdoc}
   */
  public function getJsonApiResponseForUpload(?string $target, bool $ignorePermission = false): JsonApiSerializableNodeInterface
  {
    $root = new JsonApiErrorRootNode();

    if (isset($_FILES['file'])) {
      if ($_FILES['file']['error'] === UPLOAD_ERR_OK && file_exists($_FILES['file']['tmp_name'])) {
        $file = $_FILES['file'];

        if (!empty($file)) {
          if (isset($target)) {
            $fieldOptions = $this->getFieldConfigForFieldTarget($target, $ignorePermission);

            if ($fieldOptions) {
              $fieldSettings = $fieldOptions->getSettings();
              $allowedExtensions = explode(' ', $fieldSettings['file_extensions']);
              $fileExtension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));

              if (in_array($fileExtension, $allowedExtensions)) {
                $uriScheme = $fieldSettings['uri_scheme'];
                $targetDirectory = $fieldSettings['file_directory'];

                // Cap the upload size according to the PHP limit.
                $maxFilesize = Bytes::toNumber(Environment::getUploadMaxSize());
                if (!empty($fieldSettings['max_filesize'])) {
                  $maxFilesize = min($maxFilesize, Bytes::toNumber($fieldSettings['max_filesize']));
                }

                if ($file['size'] <= $maxFilesize) {
                  $fileName = basename($_FILES['file']['name']);
                  $data = file_get_contents($_FILES['file']['tmp_name']);

                  try {
                    $file = $this->createNewFile($uriScheme, $targetDirectory, $fileName, $data);

                    $root = new JsonApiRootNode();
                    $node = $this->modelSerializer->getJsonApiNodeForModel($file);
                    $root->addNode($node);
                  } catch (Exception $e) {
                    $this->logger->error($e->getMessage() . ' ' . $e->getTraceAsString());

                    $root = new JsonApiErrorRootNode();
                    $node = new JsonApiErrorNode();
                    $node->setStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
                    $node->setCode('SERVER_ERROR');
                    $node->setDetail('Something went wrong, please try again later');
                    $root->addError($node);
                  }
                } else {
                  $node = new JsonApiErrorNode();
                  $node->setStatus(Response::HTTP_BAD_REQUEST);
                  $node->setCode('FILE_TOO_LARGE');
                  $node->setDetail('File is too large');
                  $root->addError($node);
                }
              } else {
                $node = new JsonApiErrorNode();
                $node->setStatus(Response::HTTP_BAD_REQUEST);
                $node->setCode('FILE_ERR_EXTENSION');
                $node->setDetail('File extension not allowed');
                $root->addError($node);
              }
            } else {
              $node = new JsonApiErrorNode();
              $node->setStatus(Response::HTTP_BAD_REQUEST);
              $node->setCode('FIELD_NOT_ACCESSIBLE');
              $node->setDetail('No access to the field');
              $root->addError($node);
            }
          } else {
            $node = new JsonApiErrorNode();
            $node->setStatus(Response::HTTP_BAD_REQUEST);
            $node->setCode('FILE_TARGET_MISSING');
            $node->setDetail('Target is missing');
            $root->addError($node);
          }
        } else {
          $node = new JsonApiErrorNode();
          $node->setStatus(Response::HTTP_BAD_REQUEST);
          $node->setCode('FILE_NO_FILE');
          $node->setDetail('No file provided');
          $root->addError($node);
        }
      } else if ($_FILES['file']['error'] === UPLOAD_ERR_INI_SIZE) {
        $node = new JsonApiErrorNode();
        $node->setStatus(Response::HTTP_BAD_REQUEST);
        $node->setCode('FILE_TOO_LARGE');
        $node->setDetail('File is too large');
        $root->addError($node);
      } else if ($_FILES['file']['error'] === UPLOAD_ERR_FORM_SIZE) {
        $node = new JsonApiErrorNode();
        $node->setStatus(Response::HTTP_BAD_REQUEST);
        $node->setCode('FILE_TOO_LARGE');
        $node->setDetail('File is too large');
        $root->addError($node);
      } else if ($_FILES['file']['error'] === UPLOAD_ERR_PARTIAL) {
        $node = new JsonApiErrorNode();
        $node->setStatus(Response::HTTP_BAD_REQUEST);
        $node->setCode('ERR_PARTIAL');
        $node->setDetail('File upload partial error');
        $root->addError($node);
      } else if ($_FILES['file']['error'] === UPLOAD_ERR_NO_FILE) {
        $node = new JsonApiErrorNode();
        $node->setStatus(Response::HTTP_BAD_REQUEST);
        $node->setCode('FILE_NO_FILE');
        $node->setDetail('No file provided');
        $root->addError($node);
      } else if ($_FILES['file']['error'] === UPLOAD_ERR_NO_TMP_DIR) {
        $node = new JsonApiErrorNode();
        $node->setStatus(Response::HTTP_BAD_REQUEST);
        $node->setCode('FILE_NO_TMP');
        $node->setDetail('File no temp');
        $root->addError($node);
      } else if ($_FILES['file']['error'] === UPLOAD_ERR_EXTENSION) {
        $node = new JsonApiErrorNode();
        $node->setStatus(Response::HTTP_BAD_REQUEST);
        $node->setCode('FILE_ERR_EXTENSION');
        $node->setDetail('File extension not allowed');
        $root->addError($node);
      } else if ($_FILES['file']['error'] === UPLOAD_ERR_CANT_WRITE) {
        $node = new JsonApiErrorNode();
        $node->setStatus(Response::HTTP_BAD_REQUEST);
        $node->setCode('FILE_CANT_WRITE');
        $node->setDetail('File cant write');
        $root->addError($node);
      }
    } else {
      $node = new JsonApiErrorNode();
      $node->setStatus(Response::HTTP_BAD_REQUEST);
      $node->setCode('UPLOAD_FAILED');
      $node->setDetail('File upload failed');
      $root->addError($node);
    }

    return $root;
  }

  /**
   * {@inheritdoc}
   */
  public function handleUploadForTarget(?string $target, bool $ignorePermission = false): Response
  {
    $root = $this->getJsonApiResponseForUpload($target, $ignorePermission);

    $responseCode = Response::HTTP_OK;

    if ($root instanceof JsonApiErrorRootNode) {
      $responseCode = Response::HTTP_BAD_REQUEST;

      $errors = $root->getErrors();

      if (sizeof($errors) === 1 && $errors[0]->getCode() === 'SERVER_ERROR') {
        $responseCode = Response::HTTP_INTERNAL_SERVER_ERROR;
      }
    }

    /** @var JsonApiRootNode|JsonApiErrorRootNode $root */
    return new Response(
      empty($root) ? null : json_encode($root->serialize()),
      $responseCode,
      ['Content-Type' => JsonApiRootNode::HEADER_CONTENT_TYPE]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldConfigForFieldTarget(string $target, bool $ignorePermission = false): ?FieldConfigInterface
  {
    $returnValue = null;

    $modelClasses = $this->modelService->getRegisteredModelClasses();
    $explodedTarget = explode('.', $target);

    if (!$explodedTarget) {
      return null;
    }

    if (sizeof($explodedTarget) !== 2 && sizeof($explodedTarget) !== 3) {
      return null;
    }

    $findFieldConfigForModelClass = function (string $modelClass, string $dasherizedField) use ($ignorePermission): ?FieldConfigInterface {
      $ignoreFields = $this->modelSerializer->getDefaultIgnoreFields();
      $fieldToPrettyMapping = $this->modelSerializer->getFieldsToPrettyFieldsMapping($modelClass);

      foreach ($this->modelService->getFieldDefinitions($modelClass) as $fieldName => $fieldDefinition) {
        if (in_array($fieldName, $ignoreFields)) {
          continue;
        }

        $fieldNamePretty = $fieldToPrettyMapping[$fieldName];

        if ($fieldNamePretty !== $dasherizedField) {
          continue;
        }

        if ($ignorePermission || $this->permissionService->currentUserHasFieldEditPermission($modelClass, $fieldName)) {
          return $fieldDefinition;
        }
      }
    };

    // Old way of working field target being bundle.field
    if (sizeof($explodedTarget) === 2) {
      $dasherizedModel = $explodedTarget[0];
      $dasherizedField = $explodedTarget[1];

      foreach ($modelClasses as $modelClass) {
        $dasherizedBundleKey = StringUtils::dasherize($this->modelService->getBundleKey($modelClass));

        if ($dasherizedBundleKey !== $dasherizedModel) {
          continue;
        }

        $returnValue = $findFieldConfigForModelClass($modelClass, $dasherizedField);

        break;
      }
    }
    // New way of working field target being entity.bundle.field
    else if (sizeof($explodedTarget) === 3) {
      $dasherizedEntity = $explodedTarget[0];
      $dasherizedBundle = $explodedTarget[1];
      $dasherizedField = $explodedTarget[2];

      foreach ($modelClasses as $modelClass) {
        $dasherizedEntityKey = StringUtils::dasherize($modelClass::entityType());
        $dasherizedBundleKey = StringUtils::dasherize($this->modelService->getBundleKey($modelClass));

        if ($dasherizedEntityKey !== $dasherizedEntity || $dasherizedBundleKey !== $dasherizedBundle) {
          continue;
        }

        $returnValue = $findFieldConfigForModelClass($modelClass, $dasherizedField);
      }
    }

    return $returnValue;
  }
}
