<?php

namespace Drupal\spectrum\Runnable;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Drupal;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\spectrum\Event\CronStatusUpdatedEvent;
use Drupal\spectrum\Exceptions\JobTerminateException;
use Drupal\spectrum\Model\FieldRelationship;
use Drupal\spectrum\Model\ModelInterface;
use Drupal\spectrum\Models\User;
use Drupal\spectrum\Permissions\AccessPolicy\AccessPolicyInterface;
use Drupal\spectrum\Permissions\AccessPolicy\PublicAccessPolicy;
use Drupal\spectrum\Services\ModelServiceInterface;
use Error;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * A queued job is an implementation of RunnableModel, it can be scheduled to
 * be executed on a later time. QueuedJob itself shouldn't be instantiated. It
 * should be extended with functionality We cannot mark the class abstract, as
 * on Query time for new QueuedJobs we don't know the Fully Qualified Classname
 * of the implementation
 */
class QueuedJob extends RunnableModel {

  const STATUS_QUEUED = 'Queued';

  const STATUS_RUNNING = 'Running';

  const STATUS_COMPLETED = 'Completed';

  const STATUS_FAILED = 'Failed';

  const RESCHEDULE_FROM_SCHEDULED_TIME = 'Scheduled Time';

  const RESCHEDULE_FROM_START_TIME = 'Start Time';

  const RESCHEDULE_FROM_END_TIME = 'End Time';

  protected bool $updateCronStatus = FALSE;

  protected AccountSwitcherInterface $accountSwitcher;

  public static function entityType(): string {
    return 'runnable';
  }

  public static function bundle(): string {
    return 'queued_job';
  }

  /**
   * @inheritDoc
   */
  public static function getAccessPolicy(): AccessPolicyInterface {
    return new PublicAccessPolicy();
  }

  public static function relationships() {
    parent::relationships();
    static::addRelationship(new FieldRelationship('run_as', 'field_run_as.target_id'));
  }

  /**
   * This function will be executed just before starting the execution of the
   * job. Here the status will be set to Running and the start time will be set
   * to the current time
   *
   * @return void
   */
  public final function preExecution(): void {
    // todo: inject clock
    $currentTime = new DateTime('now', new DateTimeZone('UTC'));

    $message = strtr('Job with ID: @id STARTED at @time (@title)', [
      '@id' => $this->getId(),
      '@time' => $currentTime->format('Y-m-d\TH:i:s'),
      '@title' => $this->getTitle(),
    ]);
    $this->print($message);

    $this->setStatus(QueuedJob::STATUS_RUNNING);
    $this->setStartTime($currentTime);
    $this->setEndTime(NULL);
    $this->setErrorMessage(NULL);

    $lock = DateTimeImmutable::createFromMutable($currentTime);
    $lock = $lock->add(new DateInterval("PT{$this->getMinutesToFailure()}M"));
    $this->setLock($lock);

    $this->save();

    // Check the user context we need to execute in, and switch to the provided user if necessary.
    // If no provided user, execute as anonymous

    $this->accountSwitcher = Drupal::service('account_switcher');
    if (empty($this->getRunAsUserId()) || $this->getRunAsUserId() === 0 || empty($this->fetch('run_as'))) {
      $this->accountSwitcher->switchTo(new AnonymousUserSession());
    } else {
      $account = $this->getRunAsUser()->entity;
      /** @var AccountInterface $account */
      $this->accountSwitcher->switchTo($account);
    }

    if (!$this instanceof BatchJob && $this->updateCronStatus) {
      /** @var EventDispatcher $eventDispatcher */
      $eventDispatcher = Drupal::service('event_dispatcher');
      $event = new CronStatusUpdatedEvent($this);
      $eventDispatcher->dispatch($event);
    }
  }

  public function afterInsert() {
    if ($this->updateCronStatus) {
      /** @var EventDispatcherInterface $dispatcher */
      $dispatcher = Drupal::service('event_dispatcher');
      $event = new CronStatusUpdatedEvent($this);
      $dispatcher->dispatch($event);
    }
  }

  /**
   * @return User|null
   */
  public function getRunAsUser(): ?User {
    return $this->get('run_as');
  }

  /**
   * @return integer|null
   */
  public function getRunAsUserId(): ?int {
    return $this->entity->{'field_run_as'}->target_id;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->entity->{'title'}->value;
  }

  /**
   * @param string $value
   *
   * @return self
   */
  public function setTitle(string $value): QueuedJob {
    $this->entity->{'title'}->value = $value;
    return $this;
  }

  /**
   * @return string|null
   */
  public function getVariable(): ?string {
    return $this->entity->{'field_variable'}->value;
  }

  /**
   * @param string|null $value
   *
   * @return self
   */
  public function setVariable(?string $value): QueuedJob {
    $this->entity->{'field_variable'}->value = $value;
    return $this;
  }

  /**
   * @return DateTime|null
   */
  public function getStartTime(): ?DateTime {
    if (empty($this->entity->{'field_start_time'}->value)) {
      return NULL;
    }

    return DateTime::createFromFormat(
      DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
      $this->entity->{'field_start_time'}->value,
      new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE)
    );
  }

  public function setStartTime(?DateTime $value): QueuedJob {
    if (empty($value)) {
      $this->entity->{'field_start_time'}->value = NULL;
      return $this;
    }

    $value = clone $value;

    $this->entity->{'field_start_time'}->value = $value
      ->setTimezone(new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
      ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    return $this;
  }

  public function getEndTime(): ?DateTime {
    if (empty($this->entity->{'field_end_time'}->value)) {
      return NULL;
    }

    return DateTime::createFromFormat(
      DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
      $this->entity->{'field_end_time'}->value,
      new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE)
    );
  }

  public function setEndTime(?DateTime $value): QueuedJob {
    if ($value === NULL) {
      $this->entity->{'field_end_time'}->value = NULL;
      return $this;
    }

    $value = clone $value;

    $this->entity->{'field_end_time'}->value = $value
      ->setTimezone(new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
      ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    return $this;
  }

  public function getScheduledTime(): ?DateTime {
    if (empty($this->entity->{'field_scheduled_time'}->value)) {
      return NULL;
    }

    return DateTime::createFromFormat(
      DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
      $this->entity->{'field_scheduled_time'}->value,
      new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE)
    );
  }

  public function setScheduledTime(?DateTimeInterface $dateTime): QueuedJob {
    if ($dateTime === NULL) {
      $this->entity->{'field_scheduled_time'}->value = NULL;
      return $this;
    }

    $dateTime = DateTime::createFromInterface($dateTime);

    $this->entity->{'field_scheduled_time'}->value = $dateTime
      ->setTimezone(new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
      ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    return $this;
  }

  public function setLock(?DateTimeImmutable $dateTime): static {
    if ($dateTime === NULL) {
      $this->entity->{'field_lock'}->value = NULL;
      return $this;
    }

    $this->entity->{'field_lock'}->value = $dateTime
      ->setTimezone(new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
      ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    return $this;
  }

  public function getErrorMessage(): ?string {
    return $this->entity->{'field_error_message'}->value;
  }

  public function setErrorMessage(?string $value): QueuedJob {
    $this->entity->{'field_error_message'}->value = $value;
    return $this;
  }

  public function getStatus(): string {
    return $this->entity->{'field_job_status'}->value;
  }

  public function setStatus(string $value): QueuedJob {
    $this->entity->{'field_job_status'}->value = $value;
    return $this;
  }

  public function getMinutesToFailure(): int {
    return $this->entity->{'field_minutes_to_failure'}->value;
  }

  public function setMinutesToFailure(int $value): QueuedJob {
    $this->entity->{'field_minutes_to_failure'}->value = $value;
    return $this;
  }

  public function shouldDeleteAfterCompletion(): bool {
    return $this->entity->{'field_delete_after_completion'}->value ?? FALSE;
  }

  public function setDeleteAfterCompletion(bool $value): QueuedJob {
    $this->entity->{'field_delete_after_completion'}->value = $value ? '1' : '0';
    return $this;
  }

  public function getRescheduleIn(): ?int {
    return $this->entity->{'field_reschedule_in'}->value;
  }

  public function setRescheduleIn(?int $value): QueuedJob {
    $this->entity->{'field_reschedule_in'}->value = $value;
    return $this;
  }

  public function getRescheduleFrom(): ?string {
    return $this->entity->{'field_reschedule_from'}->value;
  }

  public function setRescheduleFrom(?string $value): QueuedJob {
    $this->entity->{'field_reschedule_from'}->value = $value;
    return $this;
  }

  /**
   * Execute the job, this function should be overridden by every job, to
   * provide an implementation
   *
   * @return void
   */
  public function execute(): void {}

  /**
   * Schedule a job on a given datetime, with a possible variable.
   *
   * @param string $jobName (required) The Name of the Job
   * @param string $variable (optional) Provide a variable for the job, it can
   *   be accessed on execution time
   * @param DateTime|null $date (optional) The date you want to schedule the
   *   job on. If left blank, "now" will be chosen
   * @param string|null $entityType (optional) The related Entity Type
   * @param string|null $bundle (optional) The related bundle
   * @param string|null $modelId (optional) The related model id
   *
   * @return QueuedJob
   */
  final public static function schedule(
    string $jobName,
    string $variable = '',
    ?DateTime $date = NULL,
    ?string $entityType = NULL,
    ?string $bundle = NULL,
    ?string $modelId = NULL
  ): QueuedJob {
    $registeredJob = RegisteredJob::getByKey($jobName);

    if ($registeredJob === NULL) {
      throw new Exception('Registered Job (' . $jobName . ') not found');
    }

    if ($date === NULL) {
      $date = new DateTime('now', new DateTimeZone('UTC'));
    }

    /** @var QueuedJob $queuedJob */
    $queuedJob = $registeredJob->createJobInstance();
    $queuedJob->setTitle($jobName);

    if (!empty($variable)) {
      $queuedJob->setVariable($variable);
    }

    $queuedJob
      ->setMinutesToFailure(10)
      ->setScheduledTime($date)
      ->setRegisteredJob($registeredJob);

    if (!empty($entityType) && !empty($modelId)) {
      $queuedJob
        ->setRelatedEntity($entityType)
        ->setRelatedBundle($bundle)
        ->setRelatedModelId($modelId);
    }

    $queuedJob->save();

    return $queuedJob;
  }

  /**
   * Set the Job failed with a reason, this function should be called from
   * within the job itself, it will raise a JobTerminateException and will
   * cause the execution to terminate in a safe way.
   *
   * @param string $message
   *
   * @return never
   */
  final public function setFailed(string $message): never {
    throw new JobTerminateException($message);
  }

  /**
   * This function will be executed after execution. The status will be put on
   * Completed, and the completion time will be filled in In case the Job needs
   * to be rescheduled, the rescheduling time will be calculated, and the new
   * job will be inserted
   *
   * @return void
   */
  final public function postExecution(): void {
    // Lets not forget to switch back to the original user context
    $this->accountSwitcher->switchBack();

    // Lets put the job to completed
    $currentTime = new DateTime('now', new DateTimeZone('UTC'));
    $this->print(strtr('Job with ID: @jobId FINISHED at @finishedTime (@jobName)', [
      '@jobId' => $this->getId(),
      '@finishedTime' => $currentTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      '@jobName' => $this->getTitle(),
    ]));

    if ($this->getStatus() === QueuedJob::STATUS_RUNNING) {
      $this->setStatus(QueuedJob::STATUS_COMPLETED);
    }

    $this->setEndTime($currentTime);
    $this->save();

    if ($this->updateCronStatus) {
      /** @var EventDispatcher $eventDispatcher */
      $eventDispatcher = Drupal::service('event_dispatcher');
      $event = new CronStatusUpdatedEvent($this);
      $eventDispatcher->dispatch($event);
    }

    // And check if we need to reschedule this job
    $this->checkForReschedule();
    $this->checkForDeletion();
  }

  /**
   * Checks if the current Queued Job needs to be deleted after completion
   *
   * @return QueuedJob
   */
  private function checkForDeletion(): QueuedJob {
    if ($this->shouldDeleteAfterCompletion() && ($this->getStatus() === self::STATUS_COMPLETED || $this->getStatus() === self::STATUS_FAILED)) {
      $this->delete();
    }

    return $this;
  }

  /**
   * Checks if the current Queued Job needs to be Rescheduled
   *
   * @return QueuedJob
   */
  private function checkForReschedule(): QueuedJob {
    $rescheduleIn = $this->getRescheduleIn();
    $rescheduleFrom = $this->getRescheduleFrom();
    if (!empty($rescheduleIn) && $rescheduleIn > 0 && !empty($rescheduleFrom)) {
      $now = new DateTime('now', new DateTimeZone('UTC'));
      $newScheduledTime = NULL;

      if ($rescheduleFrom === QueuedJob::RESCHEDULE_FROM_SCHEDULED_TIME) {
        $newScheduledTime = $this->getScheduledTime();
      } else {
        if ($rescheduleFrom === QueuedJob::RESCHEDULE_FROM_START_TIME) {
          $newScheduledTime = $this->getStartTime();
        } else {
          if ($rescheduleFrom === QueuedJob::RESCHEDULE_FROM_END_TIME) {
            $newScheduledTime = $this->getEndTime();
          }
        }
      }

      $newScheduledTime->modify('+' . $rescheduleIn . ' minutes');

      if ($newScheduledTime < $now) {
        $newScheduledTime = $now;
      }

      /** @var QueuedJob $copiedJob */
      $copiedJob = $this->getCopiedModel();
      $copiedJob->setEndTime(NULL);
      $copiedJob->setStartTime(NULL);
      $copiedJob->setErrorMessage(NULL);
      $copiedJob->setCreatedDate($now);
      $copiedJob->setStatus(QueuedJob::STATUS_QUEUED);
      $copiedJob->setScheduledTime($newScheduledTime);
      $copiedJob->setLock(NULL);
      $copiedJob->save();

      $this->print(strtr('Job with ID: @jobId RESCHEDULED at @finishedTime (@jobName)', [
        '@jobId' => $copiedJob->getId(),
        '@finishedTime' => $newScheduledTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        '@jobName' => $copiedJob->getTitle(),
      ]));
    }

    return $this;
  }

  /**
   * Sets the job failed, this method will be called from within the scheduler
   * in case an Exception was raised.
   *
   * @param Exception $ex
   * @param string $message
   *
   * @return void
   */
  final public function failedExecution(?Exception $ex = NULL, string $message = NULL): void {
    // Execution failed, set the status to failed
    // Set a possible error message

    $currentTime = new DateTime('now', new DateTimeZone('UTC'));

    $this->print(strtr('Job with ID: @jobId FAILED at @finishedTime (@jobName)', [
      '@jobId' => $this->getId(),
      '@finishedTime' => $currentTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      '@jobName' => $this->getTitle(),
    ]));

    $this->setStatus(QueuedJob::STATUS_FAILED);
    $this->setEndTime($currentTime);

    if ($ex !== NULL) {
      $message = $ex->getMessage();

      if (!($ex instanceof JobTerminateException)) {
        $message = '(' . $message . ') ' . $ex->getTraceAsString();

        Drupal::logger('spectrum_cron')
          ->error($ex->getMessage() . ' ' . $ex->getTraceAsString());
      }

      $this->setErrorMessage($message);
    } else {
      if (!empty($message)) {
        $this->setErrorMessage($message);
      }
    }

    $this->save();
    $this->checkForReschedule();

    if ($ex !== NULL) {
      throw $ex;
    }
  }

  final public function runtimeError(Error $error): void {
    // Execution failed, set the status to failed
    // Set a possible error message

    $currentTime = new DateTime('now', new DateTimeZone('UTC'));

    $this->print(strtr('Job with ID: @jobId generated RUNTIME ERROR at @finishedTime (@jobName)', [
      '@jobId' => $this->getId(),
      '@finishedTime' => $currentTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      '@jobName' => $this->getTitle(),
    ]));

    $this->setStatus(QueuedJob::STATUS_FAILED);
    $this->setEndTime($currentTime);

    $message = $error->getMessage();
    $message = '(' . $message . ') ' . $error->getTraceAsString();

    Drupal::logger('spectrum_cron')->error($error->getMessage());
    Drupal::logger('spectrum_cron')->error($error->getTraceAsString());

    $this->setErrorMessage($message);
    $this->save();
    $this->checkForReschedule();

    throw $error;
  }

  public function getRelatedBundle(): ?string {
    return $this->entity->{'field_related_bundle'}->value;
  }

  public function setRelatedBundle(?string $value): self {
    $this->entity->{'field_related_bundle'}->value = $value;
    return $this;
  }

  public function getRelatedEntity(): ?string {
    return $this->entity->{'field_related_entity'}->value;
  }

  public function setRelatedEntity(?string $value): self {
    $this->entity->{'field_related_entity'}->value = $value;
    return $this;
  }

  public function getRelatedModelId(): ?string {
    return $this->entity->{'field_related_model_id'}->value;
  }

  public function setRelatedModelId(?string $value): self {
    $this->entity->{'field_related_model_id'}->value = $value;
    return $this;
  }

  public function getRelatedModel(): ?ModelInterface {
    /** @var ModelServiceInterface $service */
    $service = Drupal::service('spectrum.model');

    /** @var \Drupal\spectrum\Model\Model $modelClass */
    $modelClass = $service->getModelClassForEntityAndBundle(
      $this->getRelatedEntity(),
      $this->getRelatedBundle(),
    );

    return $modelClass::forgeById($this->getRelatedModelId());
  }

  public function setRelatedModel(ModelInterface $model): self {
    $this->setRelatedEntity($model::entityType());
    $this->setRelatedBundle($model::bundle());
    $this->setRelatedModelId($model->getId());

    return $this;
  }

  /**
   * Returns the Related Model if filled in.
   *
   * @return ModelInterface|null
   */
  public function fetchRelatedModel(): ?ModelInterface {
    $entityType = $this->getRelatedEntity();
    $bundle = $this->getRelatedBundle();
    $modelId = $this->getRelatedModelId();

    if (empty($entityType) || empty($modelId)) {
      return NULL;
    }

    /** @var ModelServiceInterface $modelService */
    $modelService = Drupal::service('spectrum.model');

    if (!$modelService->hasModelClassForEntityAndBundle($entityType, $bundle)) {
      return NULL;
    }

    /** @var \Drupal\spectrum\Model\Model $modelClass */
    $modelClass = $modelService->getModelClassForEntityAndBundle($entityType, $bundle);
    return $modelClass::forgeById($modelId);
  }

  protected static function getJobShortClassName(): string {
    return preg_replace('/^(\w+\\\)*/', '', static::class);
  }

}
