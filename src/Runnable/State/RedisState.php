<?php

declare(strict_types=1);

namespace Drupal\spectrum\Runnable\State;

use Drupal;
use Redis;

/**
 * There does not seem to be an interface that both Predis and PhpRedis
 * adhere to. For now, we've hard-coded the state to use Predis.
 * This will crash when trying to use PhpRedis.
 */
final class RedisState implements StateInterface {

  public function __construct(private readonly Redis $client) {
  }

  public function get(string $key): string {
    if (!$this->client->isConnected()) {
      Drupal::logger('groupflights')->error('not connected to redis');
    }

    return $this->client->get($key);
  }

  public function set(string $key, mixed $value): void {
    if (!$this->client->isConnected()) {
      Drupal::logger('groupflights')->error('not connected to redis');
    }

    $this->client->set($key, $value);
  }

  public function increaseBy(string $key, int $value): int {
    return $this->client->incrBy($key, $value);
  }

  public function decreaseBy(string $key, int $value): int {
    return $this->client->decrBy($key, $value);
  }

  public function delete(string $key): void {
    $this->client->del($key);
  }

}