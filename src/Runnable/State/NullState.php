<?php

declare(strict_types=1);

namespace Drupal\spectrum\Runnable\State;

final class NullState implements StateInterface {

  public function get(string $key): string {
    return '';
  }

  public function set(string $key, mixed $value): void {

  }

  public function increaseBy(string $key, int $value): int {
    return 1;
  }

  public function decreaseBy(string $key, int $value): int {
    return 0;
  }

  public function delete(string $key): void {

  }

}