<?php

namespace Drupal\spectrum\Runnable\State;

interface StateInterface {

  public function get(string $key): string;

  public function set(string $key, mixed $value): void;

  public function increaseBy(string $key, int $value): int;

  public function decreaseBy(string $key, int $value): int;

  public function delete(string $key): void;

}