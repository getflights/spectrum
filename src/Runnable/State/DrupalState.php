<?php

declare(strict_types=1);

namespace Drupal\spectrum\Runnable\State;

final class DrupalState implements StateInterface {

  public function __construct(
    private readonly \Drupal\Core\State\StateInterface $state
  ) {
  }

  public function get(string $key): string {
    return (string)$this->state->get($key);
  }

  public function set(string $key, mixed $value): void {
    $this->state->set($key, $value);
  }

  public function increaseBy(string $key, int $value): int {
    $value = (int)$this->get($key) + $value;
    $this->state->set($key, $value);

    return $value;
  }

  public function decreaseBy(string $key, int $value): int {
    $value = (int)$this->get($key) - $value;
    $this->state->set($key, $value);

    return $value;
  }

  public function delete(string $key): void {
    $this->state->delete($key);
  }

}