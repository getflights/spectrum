<?php

namespace Drupal\spectrum\Runnable\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Job annotation object.
 *
 * @Annotation
 *
 * @see \Drupal\spectrum\Services\JobService
 * @see \Drupal\spectrum\Runnable\JobInterface
 *
 * @ingroup spectrum
 */
class Job extends Plugin {

  public string $id;

  public string $description = '';

  public ?string $executor = NULL;

  /**
   * @var class-string $bulkOperationFor
   */
  public string $bulkOperationFor;

}
