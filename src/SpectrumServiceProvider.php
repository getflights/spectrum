<?php

declare(strict_types=1);

namespace Drupal\spectrum;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Site\Settings;
use Drupal\spectrum\Runnable\State\NullState;

final class SpectrumServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {
    if (Settings::get('test_mode')) {
      $mock = $container->getDefinition('spectrum.runnable.state');
      $mock->setClass(NullState::class);
      $mock->setArguments([]);

      $container->setDefinition('spectrum.runnable.state', $mock);
    }
  }

}
