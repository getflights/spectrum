<?php

namespace Drupal\spectrum\Model;

use Drupal\Core\Entity\EntityInterface;
use Drupal\spectrum\Models\File;
use Drupal\spectrum\Permissions\AccessPolicy\AccessPolicyInterface;
use Drupal\spectrum\Query\Query;

/**
 * Interface ModelInterface
 *
 * @package Drupal\spectrum\Model
 */
interface ModelInterface
{

  /**
   * Get the entity that was wrapped by this Model
   *
   * @return  EntityInterface
   */
  public function getEntity(): EntityInterface;

  /**
   * The entity type of this model (for example "node"), this should be defined
   * in every subclass
   *
   * @var string
   * @return string
   */
  public static function entityType(): string;

  /**
   * The bundle of this model (for example "article"), this should be defined
   * in every subclass
   *
   * @var string
   * @return string
   */
  public static function bundle(): string;

  /**
   * What access policy should be used to access records of this model
   *
   * @return AccessPolicyInterface
   */
  public static function getAccessPolicy(): AccessPolicyInterface;

  /**
   * @param string|NULL $relationshipName
   *
   * @return self
   */
  public function save(string $relationshipName = NULL): self;

  /**
   * @param string $relationshipName
   * @param \Drupal\spectrum\Query\Query|null $queryToCopyFrom
   *
   * @return mixed
   */
  public function fetch(string $relationshipName, ?Query $queryToCopyFrom = NULL);

  /**
   * @param string $relationshipName
   *
   * @return CollectionInterface|ModelInterface|null
   */
  public function get(string $relationshipName);

  /**
   * Returns the fully qualified ModelClass name
   *
   * @return string
   */
  public function getModelClass(): string;

  /**
   * Checks if a model is related via a fieldrelationship currently in memory
   *
   * @param string $relationshipName
   *
   * @return boolean
   */
  public function isRelatedViaFieldRelationshipInMemory(string $relationshipName): bool;

  /**
   * Checks if a model is related via a referenced relationship currently in
   * memory
   *
   * @param string $relationshipName
   *
   * @return boolean
   */
  public function isRelatedViaReferencedRelationshipInMemory(string $relationshipName): bool;

  /**
   * Returns all the current referenced relationships in memory
   *
   * @return CollectionInterface[]
   */
  public function getReferencedRelationshipsInMemory(): array;

  /**
   * Returns all the current field relationships in memory
   *
   * @return ModelInterface[]|CollectionInterface[]
   */
  public function getFieldRelationshipsInMemory(): array;

  /**
   * Create a Copy of this entity and wrap it in a Model, the IDs will be
   * blank, and all the field values will be filled in.
   *
   * @return ModelInterface
   */
  public function getCopiedModel(): ModelInterface;

  /**
   * This function loads the translations, the first found translation will be
   * used on the entity. In case no translation is found, the default language
   * will be loaded
   *
   * @param String[] $languageCodes an array containing the languagecodes you
   *   want to load on the entity
   *
   * @return self
   */
  public function loadTranslation(array $languageCodes): self;

  /**
   * Check whether the Model is new (not yet persisted to the DB)
   *
   * @return boolean
   */
  public function isNew(): bool;

  /**
   * Returns a unique identifier for this model, usually this is the ID of the
   * entity, in case the entity is not yet saved to the database, this should
   * be a unique placeholder
   *
   * @return int|string
   */
  public function getModelIdentifier();

  /**
   * Sets the unique identifier for this model
   *
   * @param string|int $identifier
   *
   * @return self
   */
  public function setModelIdentifier($identifier): self;

  /**
   * Validate the Model, or if a relationshipName was provided, the relationship
   *
   * @param string $relationshipName
   *
   * @return Validation
   */
  public function validate(string $relationshipName = NULL): Validation;

  /**
   * Clear the relationship from this Model
   *
   * @param string $relationshipName
   *
   * @return self
   */
  public function clear(string $relationshipName): self;

  /**
   * Returns the Id of the Model, this correctly handles the different
   * id-fieldnames
   *
   * @return int|string|null
   */
  public function getId();

  /**
   * Returns the UUID of the Model
   *
   * @return string
   */
  public function getUUID(): string;

  /**
   * Returns the id of the provided Relationship
   *
   * @param FieldRelationship $relationship
   *
   * @return int|string|int[]|string[]|null
   */
  public function getFieldId(FieldRelationship $relationship);

  /**
   * Refreshes the entity from the database, the wrapped entity in this
   * modelclass will be replaced with a new entity from the database Any
   * unsaved changes will be lost!
   *
   * This does not work on the entities, and will throw an Exception if tried
   *
   * @param string (optional) $relationshipName pass in the relationshipname
   *   you want to refresh, pass null if you want to refresh the current model
   *
   * @return self
   */
  public function refresh(?string $relationshipName = NULL): self;

  /**
   * Put the provided object on the provided relationship
   *
   * @param Relationship|string $relationship
   * @param ModelInterface|CollectionInterface $objectToPut
   * @param boolean $includeInOriginalModels
   *
   * @return void
   */
  public function put($relationship, $objectToPut, $includeInOriginalModels = FALSE);

  /**
   * Delete the Model from the database. This should only be used when this
   * model doesnt exists in a Collection (whether it is a relationship or not)
   * Else delete it via the Collection, so the UnitOfWork can do its job
   *
   * @return self
   */
  public function delete(): self;

  /**
   * @param string $fieldName
   * @param string $filename
   * @param string $fileContent
   *
   * @return File
   */
  public function setFileField(string $fieldName, string $filename, string $fileContent): File;

  /**
   * This method is used to add FieldConstraints at runtime to the Model
   *
   * @return void
   */
  public function constraints();

  /**
   * This method is executed by ModelApiHandlers before validation takes place.
   * (this gives you the opportunity to fill in required fields before
   * validation)
   *
   * @return void
   */
  public function beforeValidate();

  /**
   * @deprecated Use ModelTriggers instead
   * This trigger is executed by the Drupal platform before the insertion of
   * the entity takes place. this gives you the opportunity to change values on
   * the entity before being persisted to the database
   *
   * @return void
   */
  public function beforeInsert();

  /**
   * @deprecated Use ModelTriggers instead
   * This trigger is executed by the Drupal platform after the insertion of the
   * entity to the database has been done
   *
   * @return void
   */
  public function afterInsert();

  /**
   * @deprecated Use ModelTriggers instead
   * This trigger is executed by the Drupal platform before the update of the
   * entity takes place. this gives you the opportunity to change values on the
   * entity before being persisted to the database
   *
   * @return void
   */
  public function beforeUpdate();

  /**
   * @deprecated Use ModelTriggers instead
   * This trigger is executed by the Drupal platform after the entity has been
   * updated in the database
   *
   * @return void
   */
  public function afterUpdate();

  /**
   * @deprecated Use ModelTriggers instead
   * This trigger is executed by the Drupal platform before the entity will be
   * deleted, giving you the opportunity to stop the deletion
   *
   * @return void
   */
  public function beforeDelete();

  /**
   * @deprecated Use ModelTriggers instead
   * This trigger is executed by the Drupal platform after the entity has been
   * deleted, giving you the opportunity to clean up related records
   *
   * @return void
   */
  public function afterDelete();

  /**
   * Helper function for trigger methods, this way we can check if the Model
   * being inserted is new or not (we cant use the ID as this will be filled
   * in)
   *
   * @return boolean
   */
  public function isNewlyInserted(): bool;

  /**
   * This flag will be set by the trigger when the Model is created through the
   * delete triggers
   *
   * @return boolean
   */
  public function isBeingDeleted(): bool;

  /**
   * @return string|null
   */
  public function getLocalId(): ?string;

  /**
   * @param string $value
   * @return self
   */
  public function setLocalId(?string $value): self;
}
