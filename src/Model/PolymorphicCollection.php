<?php

namespace Drupal\spectrum\Model;

use Drupal;
use Drupal\spectrum\Exceptions\InvalidTypeException;
use Drupal\spectrum\Exceptions\PolymorphicException;
use Drupal\spectrum\Query\Condition;
use Drupal\spectrum\Query\EntityQuery;
use Drupal\spectrum\Query\Query;
use RuntimeException;

/**
 * This class provides an extensions of the Collection class, where in a
 * regular collection only 1 type of Model can be hold. A polymorphic
 * Collection can hold multiple model types. This is used when an Entity
 * Reference can refer to multiple different entities
 *
 * This class has limitations, not all functionality of a regular collection
 * works
 */
class PolymorphicCollection extends Collection {

  /**
   * The entity type allowed in this Collection (only models with the same
   * entity type allowed), The first added entitytype of a model is used
   *
   * @var string
   */
  private $entityType;

  /**
   * Holds the modeltypes of the models in this collection. Call
   * `setModelTypes` first to set it
   *
   * @var class-string[]
   */
  private $modelTypes = [];

  /**
   * Forces the same entity type for the models in this collection
   */
  private bool $forceSameEntityType = TRUE;

  /**
   * @param boolean $value
   *
   * @return self
   */
  public function setForceSameEntityType(bool $value): self {
    $this->forceSameEntityType = $value;
    return $this;
  }

  /**
   * Check whether all the models are of the same entity type in this collection
   *
   * @return boolean
   */
  public function sameEntityTypeForced(): bool {
    return $this->forceSameEntityType;
  }

  /**
   * This method gives you the ability to save all models in the collection.
   * Passing in a relationshipName does not work, and will throw an Exception,
   * it has no meaning for polymorphic Collections
   *
   * @param string $relationshipName DO NOT USE
   *
   * @return Collection
   */
  public function save(string $relationshipName = NULL): Collection {
    if (!empty($relationshipName)) {
      throw new PolymorphicException('Relationship path "' . $relationshipName . '" has no meaning for polymorphic collections');
    }

    return parent::save();
  }

  /**
   * This method gives you the ability to validate all models in the
   * collection.
   * Passing in a relationshipName does not work, and will throw an Exception,
   * it has no meaning for polymorphic Collections
   *
   * @param string $relationshipName DO NOT USE
   *
   * @return Validation
   */
  public function validate(string $relationshipName = NULL): Validation {
    if (!empty($relationshipName)) {
      throw new PolymorphicException('Relationship path "' . $relationshipName . '" has no meaning for polymorphic collections');
    }

    return parent::validate();
  }

  /**
   * Fetches the provided relationship name. If this polymorphic collection is
   * empty, an empty collection will be returned
   *
   * @param string $relationshipName
   *
   * @return Collection
   */
  public function fetch(string $relationshipName, ?Query $queryToCopyFrom = NULL): Collection {
    $this->checkRelationship($relationshipName);

    $resultCollection = Collection::forgeNew(NULL);

    if (sizeof($this) === 0) {
      return $resultCollection;
    }

    return parent::fetch($relationshipName, $queryToCopyFrom);
  }

  /**
   * Puts a new Model or a Collection in a Polymorphic Collection, these can be
   * different model types, however they must be of the same Entity. In Drupal
   * an Entity Reference field, can reference every bundle in the same Entity,
   * but not different Entities. For Example, you can put a "node_article" and
   * a "node_basic_page" in the same Polymorphic Collection, but no
   * "node_article" and a "user"
   *
   * @param ModelInterface|CollectionInterface $objectToPut
   *
   * @return Collection
   */
  public function put(ModelInterface|CollectionInterface $objectToPut): Collection {
    if ($objectToPut instanceof Collection) {
      foreach ($objectToPut as $model) {
        $this->put($model);
      }
    } else {
      $model = $objectToPut;

      if ($this->sameEntityTypeForced()) {
        // it is only possible to have models with a shared entity in a collection
        if (empty($this->entityType)) {
          $this->entityType = $model::entityType();
        } else {
          if ($this->entityType !== $model::entityType()) {
            throw new PolymorphicException('Only models with a shared entity type are allowed in a polymorphic collection');
          }
        }
      }

      // due to the the shared entity constraint, the key of polymorphic collections is unique,
      // because in drupal ids are unique over different bundles withing the same entity
      // so we can just use the parent addModelToModels and addModelToOriginalModels function, we won't have any conflicts there
      $this->addModelToModels($model);
    }

    return $this;
  }

  /**
   * Puts a new Model or a Collection in a Polymorphic Collection, these can be
   * different model types, however they must be of the same Entity. In Drupal
   * an Entity Reference field, can reference every bundle in the same Entity,
   * but not different Entities. For Example, you can put a "node_article" and
   * a "node_basic_page" in the same Polymorphic Collection, but no
   * "node_article" and a "user"
   *
   * @param object $objectToPut
   *
   * @return Collection
   */
  public function putOriginal($objectToPut): Collection {
    if ($objectToPut instanceof Collection) {
      foreach ($objectToPut as $model) {
        $this->putOriginal($model);
      }
    } else {
      $model = $objectToPut;

      if ($this->sameEntityTypeForced()) {
        // it is only possible to have models with a shared entity in a collection
        if (empty($this->entityType)) {
          $this->entityType = $model::entityType();
        } else {
          if ($this->entityType !== $model::entityType()) {
            throw new PolymorphicException('Only models with a shared entity type are allowed in a polymorphic collection');
          }
        }
      }

      // due to the the shared entity constraint, the key of polymorphic collections is unique,
      // because in drupal ids are unique over different bundles withing the same entity
      // so we can just use the parent addModelToModels and addModelToOriginalModels function, we won't have any conflicts there
      $this->addModelToOriginalModels($model);
    }

    return $this;
  }

  /**
   * @return ModelInterface
   * @deprecated
   * PutNew doesnt work for polymporhic collections, as it is unknown what type
   *   should be created.
   *
   */
  public function putNew(): ModelInterface {
    throw new PolymorphicException('PutNew has no meaning for polymorphic collections, we can\'t know the type of model to create');
  }

  /**
   * Returns a Collection of all the models of a certain type in this
   * polymorphic collection
   */
  public function getModelsByModelClass(string $modelClass): Collection {
    $resultCollection = Collection::forgeNew($modelClass);

    foreach ($this->models as $model) {
      if ($model instanceof $modelClass) {
        $resultCollection->put($model);
      }
    }

    return $resultCollection;
  }

  /**
   * Returns a collection containing all the models of the provided
   * relationship. If there are no models in this polymorphic collection it
   * returns an empty collection
   *
   * @param string $relationshipName
   *
   * @return Collection
   */
  public function get(string $relationshipName): Collection {
    $this->checkRelationship($relationshipName);

    $resultCollection = Collection::forgeNew(NULL);

    if (sizeof($this) === 0) {
      return $resultCollection;
    }

    return parent::get($relationshipName);
  }

  /**
   * @param string $relationshipName
   *
   * @return boolean
   * @deprecated
   * Checking if a relationship exists on a Polymorphic Collection has no
   *   meaning, it will throw an Exception
   *
   */
  public function hasRelationship(string $relationshipName): bool {
    throw new PolymorphicException('hasRelationship has no meaning for polymorphic collections');
  }

  /**
   * Returns the different modelTypes in this collection
   *
   * @return string[]
   */
  public function getModelTypesInModels(): array {
    $modelTypes = [];

    foreach ($this->models as $model) {
      $modelTypes[] = get_class($model);
    }

    return array_filter(array_unique($modelTypes));
  }

  /**
   * Sets the modelTypes
   *
   * @return PolymorphicCollection
   */
  public function setModelTypes(): PolymorphicCollection {
    $this->modelTypes = $this->getModelTypesInModels();
    return $this;
  }

  /**
   * Returns a list of all the modeltypes. Mid you, this can be empty, call
   * setModelTypes first
   *
   * @return string[]
   */
  public function getModelTypes(): array {
    return $this->modelTypes;
  }

  /**
   * Returns the first modelType of this polymorphic collection
   *
   * @return string
   */
  public function getModelType(): string {
    if (empty($this->modelTypes) && sizeof($this->models) > 0) {
      $this->setModelTypes();
    }

    return $this->modelTypes[0];
  }

  /**
   * Checks if a relationship exists for this polymorphic collection
   * The relationship name must exists on every modeltype, and must be of the
   * same type
   *
   * @param string $relationship
   *
   * @return void
   */
  public function checkRelationship(string $relationshipName) {
    if (empty($this->modelTypes) && sizeof($this->models) === 0) {
      return;
    }
    if (empty($this->modelTypes) && sizeof($this->models) > 0) {
      $this->setModelTypes();
    }

    /** @var Relationship $relationship */
    $relationship = NULL;
    foreach ($this->modelTypes as $modelType) {
      if (!$modelType::hasDeepRelationship($relationshipName)) {
        throw new InvalidTypeException(strtr('Relationship @relationship does not exist on @modelType', [
          '@relationship' => $relationshipName,
          '@modelType' => $modelType,
        ]));
      }

      if (empty($relationship)) {
        $relationship = $modelType::getDeepRelationship($relationshipName);
      } else {
        /** @var Relationship $checkRelationship */
        $checkRelationship = $modelType::getDeepRelationship($relationshipName);

        if ($checkRelationship->isPolymorphic) {
          if (count(array_diff($checkRelationship->getPolymorphicModelTypes(), $relationship->getPolymorphicModelTypes()))) {
            throw new InvalidTypeException(strtr('Not all relationships with name @relationship are for the same modelTypes', [
              '@relationship' => $relationshipName,
            ]));
          }
        } elseif ($checkRelationship->getModelType() !== $relationship->getModelType()) {
          throw new InvalidTypeException(strtr('Not all relationships with name @relationship are for the same modelTypes', [
            '@relationship' => $relationshipName,
          ]));
        }
      }
    }
  }

  /**
   * Add a Model to the model array
   *
   * @param ModelInterface $model
   *
   * @return self
   */
  protected function addModelToModels(ModelInterface $model): self {
    if (!array_key_exists($model->getModelIdentifier(), $this->models)) {
      $this->models[$model->getModelIdentifier()] = $model;
    }
    if (!array_key_exists($model->bundle(), $this->modelTypes)) {
      $this->modelTypes[] = $model::class;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildArrayByFieldName(string $fieldName): array {
    $modelColumns = [];
    foreach ($this->modelTypes as $modelType) {
      $fieldDefinition = Drupal::service('spectrum.model')
        ->getFieldDefinition($modelType, $fieldName);
      switch ($fieldDefinition->getType()) {
        case 'address':
        case 'geolocation':
          throw new InvalidTypeException('You cant build an array by this field type');
        case 'entity_reference':
        case 'entity_reference_revisions':
        case 'file':
        case 'image':
          $modelColumns[$modelType] = 'target_id';
          break;
        case 'link':
          $modelColumns[$modelType] = 'uri';
          break;
        default:
          $modelColumns[$modelType] = 'value';
          break;
      }
    }

    $array = [];
    foreach ($this->models as $model) {
      if (!array_key_exists($model::class, $modelColumns)) {
        throw new RuntimeException('Model missing from modelTypes');
      } else {
        $column = $modelColumns[$model::class];
      }
      $key = $model->entity->$fieldName->$column;
      $array[$key] = $model;
    }

    return $array;
  }

  /**
   * @param class-string $modelType
   * @param string[] $ids
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  protected static function fetchEntities(string $modelType, array $ids): array {
    /** @var \Drupal\spectrum\Services\ModelServiceInterface $modelService */
    $modelService = Drupal::service('spectrum.model');

    $query = new EntityQuery($modelType::entityType());

    $query->addCondition(new Condition(
      $modelService->getIdField($modelType),
      'IN',
      $ids
    ));

    return $query->fetch();
  }

}
