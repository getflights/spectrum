<?php

namespace Drupal\spectrum\Model;

use Drupal;
use Drupal\spectrum\Exceptions\InvalidFieldException;
use Drupal\spectrum\Query\Condition;
use Drupal\spectrum\Query\EntityQuery;

/**
 * A Field Relationship is a relationship that has a column on the enitty
 * you're defining the relationship on. It adds a foreign key with the ID of
 * another entity
 */
class FieldRelationship extends Relationship {

  /**
   * The name of the relationship field on the entity
   */
  public string $relationshipField;

  /**
   * Indicator whether this relationship is polymorphic (multiple bundles
   * allowed) or not
   */
  public bool $isPolymorphic = FALSE;

  /**
   * An array containing the list of fully qualified classnames of all the
   * polymoprhic model types
   *
   * @var string[]
   */
  public array $polymorphicModelTypes = [];

  /**
   * THe first fully qualified classname of the model in a polymorphic
   * fieldrelationship
   */
  private string $firstModelType;

  /**
   * Cardinality is the maximum number of references allowed for the field.
   */
  public int $fieldCardinality;

  protected string $class;

  /**
   * FieldRelationship constructor.
   *
   * @param string $relationshipName
   * @param string $relationshipField The fieldname of the drupal entity that
   *   stores this relationship, should be `field.column` for example
   *   `field_user.target_id`
   * @param int $cascade
   */
  public function __construct(
    string $relationshipName,
    string $relationshipField,
    int $cascade = 0
  ) {
    parent::__construct($relationshipName, $cascade);
    $this->relationshipField = $relationshipField;
  }

  /**
   * Returns a Condition with the relationship modeltype idfield already filled
   * in as the field
   *
   * @return Condition
   */
  public function getCondition(): Condition {
    $modelType = $this->firstModelType;
    return new Condition(Drupal::service('spectrum.model')
      ->getIdField($modelType), 'IN', NULL);
  }

  /**
   * Returns an EntityQuery for the relationship
   *
   * @return EntityQuery
   */
  public function getRelationshipQuery(): EntityQuery {
    $modelType = $this->firstModelType;
    if ($this->isPolymorphic) {
      return $modelType::getEntityQuery();
    } else {
      return $modelType::getModelQuery();
    }
  }

  /**
   * Set the metadata of the relationship from the Drupal Field definitions,
   * metadata includes the types (whether the relationship is polymorphic or
   * not) The fieldCardinality (whether 1 or multiple values can be set on the
   * relationship), the entitytype and bundle
   *
   * @return void
   */
  protected function setRelationshipMetaData(): void {
    $relationshipSource = $this->relationshipSource;

    $key = strtr('@source--@field', [
      '@source' => md5($relationshipSource),
      '@field' => md5($this->getField()),
    ]);

    if ($spectrumDefinition = Drupal::cache('spectrum')->get($key)) {
      $spectrumDefinition = unserialize($spectrumDefinition->data, [
        'allowed_classes' => [FieldDefinition::class],
      ]);
    } else {
      // First we will get the field Definition to read our metadata from
      $fieldDefinition = Drupal::service('spectrum.model')
        ->getFieldDefinition($relationshipSource, $this->getField());

      if ($fieldDefinition === NULL) {
        throw new InvalidFieldException('Field ' . $this->getField() . ' not found on modeltype: ' . $relationshipSource);
      }

      $fieldSettings = $fieldDefinition->getItemDefinition()->getSettings();

      $spectrumDefinition = new FieldDefinition(
        $fieldSettings['target_type'],
        $fieldSettings['handler_settings']['target_bundles'] ?? [],
        $fieldDefinition->getFieldStorageDefinition()->getCardinality(),
      );

      Drupal::cache('spectrum')->set($key, serialize($spectrumDefinition));
    }

    // Here we decide if our relationship is polymorphic or for a single entity/bundle type
    $relationshipEntityType = $spectrumDefinition->targetType;
    $relationshipBundle = NULL;

    if ($spectrumDefinition->targetBundles !== []) {
      // with all entity references this shouldn't be a problem, however, in case of 'user', this is blank
      // Luckily we handle this correctly in getModelClassForEntityAndBundle
      $relationshipBundle = array_values($spectrumDefinition->targetBundles)[0];
    }

    $this->firstModelType = Drupal::service('spectrum.model')
      ->getModelClassForEntityAndBundle($relationshipEntityType, $relationshipBundle);

    if (sizeof($spectrumDefinition->targetBundles) > 1) {
      $this->isPolymorphic = TRUE;
      $this->modelType = NULL;

      foreach ($spectrumDefinition->targetBundles as $targetBundle) {
        $this->polymorphicModelTypes[] = Drupal::service('spectrum.model')
          ->getModelClassForEntityAndBundle($relationshipEntityType, $targetBundle);
      }
    } else {
      $this->modelType = $this->firstModelType;
    }

    // Next we set the cardinality of the field, either we have a single reference or multiple references (single parent / multiple parents)
    $this->fieldCardinality = $spectrumDefinition->cardinality;
  }

  /**
   * Returns the field part of the relationship (for example
   * field_user.target_id will return "field_user")
   *
   * @return string
   */
  public function getField(): string {
    $positionOfDot = strpos($this->relationshipField, '.');
    return $positionOfDot ? substr($this->relationshipField, 0, $positionOfDot) : $this->relationshipField;
  }

  /**
   * Returns the column of the relationship (for example field_user.target_id
   * will return "target_id")
   *
   * @return string
   */
  public function getColumn(): string {
    $positionOfDot = strpos($this->relationshipField, '.');
    return substr($this->relationshipField, $positionOfDot + 1); // exclude the "." so +1
  }

  /**
   * Returns all the polymorphic model types
   *
   * @return string[]
   */
  public function getPolymorphicModelTypes(): array {
    return $this->polymorphicModelTypes;
  }

  /**
   * Magic getter for ease of access
   *
   * @param string $property
   *
   * @return void
   */
  public function __get($property) {
    if (property_exists($this, $property)) {
      return $this->$property;
    }

    switch ($property) {
      case "column":
        return $this->getColumn();
      case "field":
        return $this->getField();
      case "isSingle":
        return $this->fieldCardinality === 1;
      case "isMultiple":
        return $this->fieldCardinality !== 1;
      case "isUnlimited":
        return $this->fieldCardinality === -1;
    }
  }

  /**
   * @return string
   */
  public function getFirstModelClass(): string {
    return $this->firstModelType;
  }

  /**
   * @return integer
   */
  public function getFieldCardinality(): int {
    return $this->fieldCardinality;
  }

}
