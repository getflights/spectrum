<?php

namespace Drupal\spectrum\Model;

use ArrayIterator;
use DateTime;
use Drupal;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\spectrum\Exceptions\CascadeNoDeleteException;
use Drupal\spectrum\Exceptions\InvalidEntityException;
use Drupal\spectrum\Exceptions\InvalidFieldException;
use Drupal\spectrum\Exceptions\InvalidTypeException;
use Drupal\spectrum\Exceptions\ModelNotFoundException;
use Drupal\spectrum\Exceptions\NotImplementedException;
use Drupal\spectrum\Exceptions\PolymorphicException;
use Drupal\spectrum\Exceptions\RelationshipNotDefinedException;
use Drupal\spectrum\Models\File;
use Drupal\spectrum\Models\Image;
use Drupal\spectrum\Permissions\AccessPolicy\AccessPolicyInterface;
use Drupal\spectrum\Query\BundleQuery;
use Drupal\spectrum\Query\Condition;
use Drupal\spectrum\Query\EntityQuery;
use Drupal\spectrum\Query\ModelQuery;
use Drupal\spectrum\Query\Query;
use Drupal\spectrum\Services\ModelServiceInterface;
use Error;
use ReflectionClass;
use ReflectionMethod;
use RuntimeException;

/**
 * A Model is a wrapper around a Drupal Entity, which provides extra
 * functionality. and an easy way of fetching and saving it to the database. It
 * also provides functionality to correctly fetch related records, defined by
 * Relationships (which are linked through an EntityReference) and correctly
 * insert, update, delete and validate entities by respecting the UnitOfWork
 * design pattern.
 *
 * Together with Collection and Relationship, this is the Core of the Spectrum
 * framework This functionality is loosely based on BookshelfJS
 * (http://bookshelfjs.org/)
 */
abstract class Model implements ModelInterface
{

  /**
   * {@inheritdoc}
   */
  public abstract static function entityType(): string;

  /**
   * {@inheritdoc}
   */
  public abstract static function bundle(): string;

  /**
   * {@inheritdoc}
   */
  public abstract static function getAccessPolicy(): AccessPolicyInterface;

  /**
   * Here are the model class mapping stored, with the entitytype/bundle as
   * key, and the fully qualified model classname as value This is to get
   * around the shared scope of multiple Models on the abstract superclass
   * Model
   *
   * @var array
   */
  public static $modelClassMapping = NULL;

  /**
   * This array will hold the defined relationships with as key the fully
   * qualified classname of the model, and as value the different defined
   * relationships.
   *
   * @var Relationship[]|array
   */
  public static array $relationships = [];

  /**
   * This is a incrementing helper variable to assign temporary keys to models
   * that havent been inserted yet
   *
   * @var integer
   */
  public static int $keyIndex = 1;

  /**
   * This array holds a mapping between the requested modeltypes and the
   * registered modeltypes Because a ModelType can have its own implementation
   * per system, we have to look up the registered ModelTypes compared to the
   * one being requested The key is the one being requested, the value is the
   * registered type.
   *
   * @var array
   */
  protected static array $cachedModelTypes = [];

  /**
   * {@inheritdoc}
   */
  public EntityInterface $entity;

  /**
   * The unique key for this model used by Collections, normally this is the
   * Entity ID, in case no id exists (new records) use a temp key
   *
   * @var string|int|null
   */
  public $key;

  /**
   * This var has no functional use. however, in practice this is often used in
   * different scenarios to select a model in a list for example
   *
   * @var boolean
   */
  public bool $selected = FALSE;

  /**
   * This flag will be set by the trigger when the Model is created through the
   * insert triggers
   *
   * @var boolean
   */
  private bool $__isNewlyInserted = FALSE;

  /**
   * This flag will be set by the trigger when the Model is created through the
   * delete triggers
   *
   * @var boolean
   */
  private bool $__isBeingDeleted = FALSE;

  /**
   * An array containing all the FieldRelationships to this Entity, with as Key
   * the relationship name The value will most likely be a Model, but can also
   * be a Collection in case of a entity reference field with multiple values
   * (fieldCardinality > 0)
   *
   * @var ModelInterface[]|CollectionInterface[]
   */
  protected array $relatedViaFieldOnEntity = [];

  /**
   * An array containing all the ReferencedRelationships to this Entity, with
   * as Key the relationship name The key is a string with the relationship
   * name, and the value will always be a collection
   *
   * @var CollectionInterface[]
   */
  protected array $relatedViaFieldOnExternalEntity = [];

  /**
   * A local Id that is mirrored between API calls, this isn't persisted in the DB, it is something a front-end can provide, and will be mirrored back in responses
   *
   * @var string|null
   */
  protected ?string $localId = null;

  /**
   * {@inheritdoc}
   */
  public function getLocalId(): ?string
  {
    return $this->localId;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocalId(?string $value): self
  {
    $this->localId = $value;
    return $this;
  }

  /**
   * In the constructor for the Model, an drupal entity must be provided
   *
   * @param EntityInterface $entity
   */
  public function __construct(EntityInterface $entity)
  {
    $this->entity = $entity;

    // TODO: implement __spectrumModel for Triggers
    // We set the new instance of the Model on the entity, this way we can reuse the model state in triggers
    //$entity->__spectrumModel = $this;

    $id = $this->getId();

    if (isset($id)) {
      $this->setModelIdentifier($id);
    } else {
      $this->setModelIdentifier(static::getNextKey());
    }
  }

  /**
   * {@inheritdoc}
   */
  final public function isNew(): bool
  {
    return empty($this->getId());
  }

  /**
   * Save the model, or if a relationshipName was passed, get the relationship
   * and save it
   *
   * @param string|NULL $relationshipName
   *
   * @return self
   */
  public function save(string $relationshipName = NULL): self
  {
    if (empty($relationshipName)) {
      $isNew = $this->isNew();
      $this->entity->save();

      if ($isNew) {
        $this->setFieldForReferencedRelationships();
        $this->updateKeys();
      }
    } else {
      $this->get($relationshipName)->save();
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  final public function delete(): self
  {
    if (!$this->isNew()) {
      $this->entity->delete();
    }

    return $this;
  }

  /**
   * This function will be called from the triggers to set the access policy on
   * the model that is being saved.
   */
  final public function setAccessPolicy(): void
  {
    $accessPolicy = static::getAccessPolicy();

    if ($accessPolicy->shouldSetAccessPolicy($this)) {
      $accessPolicy->onSave($this);
    }
  }

  /**
   * This function will be called from the triggers to remove the model from
   * the access policy (because the model is being deleted)
   *
   * @return void
   */
  final public function unsetAccessPolicy(): void
  {
    static::getAccessPolicy()->onDelete($this);
  }

  /**
   * Update the Key of this Model with the ID, and update the key in every
   * Relationship (and inverse) where this Model was already put This is used
   * when a temporary Key is generated, the Model is saved, and the Key is
   * updated to the ID of the model
   *
   * @return self
   */
  private function updateKeys(): self
  {
    // we start of by reputting our keys
    $oldKey = $this->getModelIdentifier();
    $this->setModelIdentifier($this->getId());
    // This method updates the current key, and all the inverse keys as well
    $relationships = static::getRelationships();
    foreach ($relationships as $relationship) {
      if ($relationship instanceof FieldRelationship) {
        // first we check if it is a single or multipel value field relationship
        if ($relationship->isMultiple) {
          // If we have a relationship where multiple values can be in a field
          // we must get every value, loop over it, and update the referencing collection on each value
          $referencedModels = $this->get($relationship);

          foreach ($referencedModels as $referencedModel) {
            $referencedEntityType = $referencedModel->entity->getEntityTypeId();
            $referencedEntityBundle = empty($referencedModel->entity->type) ? NULL : $referencedModel->entity->{'type'}->target_id;
            $referencedModelType = static::service()
              ->getModelClassForEntityAndBundle($referencedEntityType, $referencedEntityBundle);

            // we must also check for an inverse relationship and, if found, put the inverse as well
            $referencedRelationship = $referencedModelType::getReferencedRelationshipForFieldRelationship($relationship);

            // Sometimes no Referenced relationship is defined, no need to set ids when it doesnt exist
            if ($referencedRelationship) {
              $referencingCollectionOnReferencedModel = $referencedModel->get($referencedRelationship);
              $referencingCollectionOnReferencedModel->replaceOldModelKey($oldKey, $this->getModelIdentifier());
            }
          }
        } else {
          // It is a single value, so we just need to get the inverse referencing collection, and update the key
          $referencedModel = $this->get($relationship);

          if (!empty($referencedModel)) {
            $referencedEntityType = $referencedModel->entity->getEntityTypeId();
            $referencedEntityBundle = empty($referencedModel->entity->type) ? NULL : $referencedModel->entity->{'type'}->target_id;
            $referencedModelType = static::service()
              ->getModelClassForEntityAndBundle($referencedEntityType, $referencedEntityBundle);

            // we must also check for an inverse relationship and, if found, put the inverse as well
            $referencedRelationship = $referencedModelType::getReferencedRelationshipForFieldRelationship($relationship);

            if (!empty($referencedRelationship)) // referencedRelationship is optional
            {
              $referencingCollectionOnReferencedModel = $referencedModel->get($referencedRelationship);
              $referencingCollectionOnReferencedModel->replaceOldModelKey($oldKey, $this->getModelIdentifier());
            }
          }
        }
      } else {
        if ($relationship instanceof ReferencedRelationship) {
          // This is a little complicated, we only need to update the keys when:
          // - The field relationship of the inverse contains multiple values
          // In that case, the value on the inverse is a collection, and this model will be stored there with its key
          // In all other cases, a single model is stored in the relationship arrays, where no key is used

          $inverseRelationship = $relationship->fieldRelationship;
          if ($inverseRelationship->isMultiple) {
            // we get the collection keeping all the models that refer to this model via a field
            $referencingModels = $this->get($relationship);
            foreach ($referencingModels as $referencingModel) {
              // Now we get the collection on our referencing model, where this model should be an item in
              $inverseReferencedCollection = $referencingModel->get($inverseRelationship);
              $inverseReferencedCollection->replaceOldModelKey($oldKey, $this->getModelIdentifier());
            }
          }
        }
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public final function clear(string $relationshipName): self
  {
    $relationship = static::getRelationship($relationshipName);
    if ($relationship instanceof FieldRelationship) {
      unset($this->relatedViaFieldOnEntity[$relationshipName]);
    } else {
      if ($relationship instanceof ReferencedRelationship) {
        unset($this->relatedViaFieldOnExternalEntity[$relationshipName]);
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public final function validate(string $relationshipName = NULL): Validation
  {
    if (empty($relationshipName)) {
      $validation = new Validation($this);
      // next we do a workaround for entity reference fields, referencing entities that are deleted
      $validation->processInvalidReferenceConstraints();
      return $validation;
    }

    return $this->get($relationshipName)->validate();
  }

  /**
   * This is the UnitOfWork Design Pattern in practice
   * This method sets the field with the newly created ID upon inserting a new
   * record This is important for when you have multiple related models in
   * memory who haven't been inserted and are just related in memory, by
   * setting the ID we know how to relate them in the DB
   *
   * @return self
   */
  private function setFieldForReferencedRelationships(): self
  {
    $relationships = static::getRelationships();
    foreach ($relationships as $relationship) {
      if ($relationship instanceof ReferencedRelationship) {
        $referencedRelationship = $this->get($relationship->getName());
        if (!empty($referencedRelationship)) {
          if ($referencedRelationship instanceof CollectionInterface) {
            foreach ($referencedRelationship as $referencedModel) {
              $referencedModel->put($relationship->fieldRelationship, $this);
            }
          } else {
            if ($referencedRelationship instanceof ModelInterface) {
              $referencedRelationship->put($relationship->fieldRelationship, $this);
            }
          }
        }
      }
    }

    return $this;
  }

  /**
   * Fetch the relationship from the Database
   *
   * @param string $relationshipName
   * @param Query $queryToCopyFrom (optional) add a query to fetch, to limit
   *   the amount of results when fetching, all base conditions, conditions and
   *   conditiongroups will be add to the fetch query
   *
   * @return ModelInterface|CollectionInterface|null
   */
  final public function fetch(string $relationshipName, ?Query $queryToCopyFrom = NULL)
  {
    $returnValue = NULL;

    $lastRelationshipNameIndex = strrpos($relationshipName, '.');

    if (empty($lastRelationshipNameIndex)) // relationship name without extra relationships
    {
      $relationship = static::getRelationship($relationshipName);

      $relationshipQuery = $relationship->getRelationshipQuery();

      // Lets see if we need to copy in some default conditions
      if (!empty($queryToCopyFrom)) {
        $relationshipQuery->copyConditionsFrom($queryToCopyFrom);
        $relationshipQuery->copySortOrdersFrom($queryToCopyFrom);
        $relationshipQuery->setUserIdForAccessPolicy($queryToCopyFrom->getUserIdForAccessPolicy());
        $relationshipQuery->setAccessPolicy($queryToCopyFrom->getAccessPolicy());

        if ($queryToCopyFrom->hasLimit()) {
          $relationshipQuery->setRange($queryToCopyFrom->getRangeStart(), $queryToCopyFrom->getRangeLength());
        }
      }

      $relationshipCondition = $relationship->getCondition();

      if ($relationship instanceof FieldRelationship) {
        $fieldId = $this->getFieldId($relationship);

        if (!empty($fieldId)) {
          // we start of by checking for multiple or single values allowed
          // in case of a single, we'll just put a single Model
          // else we'll put a collection of models

          if (is_array($fieldId)) // multiple values
          {
            $relationshipCondition
              ->setValue($fieldId)
              ->setOperator('IN');

            $relationshipQuery->addCondition($relationshipCondition);
            $referencedEntities = $relationshipQuery->fetch();

            if (!empty($referencedEntities)) {
              $referencedModelType = NULL;
              foreach ($referencedEntities as $referencedEntity) {
                $referencedModel = NULL;
                if ($relationship->isPolymorphic || empty($referencedModelType)) {
                  // if the relationship is polymorphic we can get multiple bundles, so we must define the modeltype based on the bundle and entity of the current looping entity
                  // or if the related modeltype isn't set yet, we must set it once
                  $referencedEntityType = $referencedEntity->getEntityTypeId();
                  $referencedEntityBundle = empty($referencedEntity->type) ? NULL : $referencedEntity->type->target_id;
                  $referencedModelType = static::service()
                    ->getModelClassForEntityAndBundle($referencedEntityType, $referencedEntityBundle);
                }

                // now that we have a model, lets put them one by one
                $referencedModel = $referencedModelType::forgeByEntity($referencedEntity);
                $returnValue = $this->put($relationship, $referencedModel, TRUE);
              }

              // And finally sort it according to the field delta
              $this->sortFieldRelationshipByFieldDelta($relationship);
            }
          } else // single value
          {
            $relationshipCondition
              ->setValue($fieldId)
              ->setOperator('=');

            $relationshipQuery->addCondition($relationshipCondition);
            $referencedEntity = $relationshipQuery->fetchSingle();

            if (!empty($referencedEntity)) {
              // if the relationship is polymorphic we can get multiple bundles, so we must define the modeltype based on the bundle and entity of the fetched entity
              $referencedEntityType = $referencedEntity->getEntityTypeId();
              $referencedEntityBundle = empty($referencedEntity->type) ? NULL : $referencedEntity->type->target_id;
              $referencedModelType = static::service()
                ->getModelClassForEntityAndBundle($referencedEntityType, $referencedEntityBundle);

              // now that we have a model, lets put them one by one
              $referencedModel = $referencedModelType::forgeByEntity($referencedEntity);
              $returnValue = $this->put($relationship, $referencedModel, TRUE);
            }
          }
        }
      } else {
        if ($relationship instanceof ReferencedRelationship) {
          $id = $this->getId();
          if (!empty($id)) // fetching referenced relationships for new records is not possible
          {
            $relationshipCondition->setValue([$id]);
            $relationshipQuery->addCondition($relationshipCondition);
            $referencingEntities = $relationshipQuery->fetch();

            if (!empty($referencingEntities)) {
              $referencingModelType = NULL;
              foreach ($referencingEntities as $referencingEntity) {
                $referencingModel = NULL;
                if (empty($referencingModelType)) {
                  // if the referencing modeltype isn't set yet, we must set it once
                  $referencingEntityType = $referencingEntity->getEntityTypeId();
                  $referencingEntityBundle = empty($referencingEntity->type) ? NULL : $referencingEntity->type->target_id;
                  $referencingModelType = static::service()
                    ->getModelClassForEntityAndBundle($referencingEntityType, $referencingEntityBundle);
                }

                // now that we have a model, lets put them one by one
                $referencingModel = $referencingModelType::forgeByEntity($referencingEntity);
                $returnValue = $this->put($relationship, $referencingModel, TRUE);
                $referencingModel->put($relationship->fieldRelationship, $this, TRUE);
              }
            }
          }
        }
      }
    } else {
      $secondToLastRelationshipName = substr($relationshipName, 0, $lastRelationshipNameIndex);
      $resultCollection = $this->get($secondToLastRelationshipName);
      if (!empty($resultCollection)) {
        $lastRelationshipName = substr($relationshipName, $lastRelationshipNameIndex + 1);
        $returnValue = $resultCollection->fetch($lastRelationshipName, $queryToCopyFrom);
      }
    }

    return $returnValue;
  }

  /**
   * Sorts a multi FieldRelationship by the field delta
   *
   * @param FieldRelationship $relationship
   *
   * @return self
   */
  public function sortFieldRelationshipByFieldDelta(FieldRelationship $relationship): self
  {
    /** @var CollectionInterface $collection */
    if ($relationship->getFieldCardinality() !== 1 && $collection = $this->get($relationship)) {
      // First we get the ids from the field, they will be sorted according to the field delta
      $fieldId = $this->getFieldId($relationship);
      // We sort by keys with uksort, and use the values of the fieldId that are returned in order
      $collection->sort(fn(ModelInterface $model1, ModelInterface $model2) => array_search($model1->getModelIdentifier(), $fieldId) <=> array_search($model2->getModelIdentifier(), $fieldId));
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public final function getModelClass(): string
  {
    return get_class($this);
  }

  /**
   * Returns the short model name (name of the class without namespace)
   *
   * @return string
   */
  public final function getShortModelName(): string
  {
    return (new ReflectionClass($this))->getShortName();
  }

  /**
   * Returns the provided relationship on this Model
   *
   * @param string|Relationship $relationship
   *
   * @return ModelInterface|CollectionInterface|null
   */
  public final function get($relationship)
  {
    $firstRelationshipNameIndex = NULL;
    if (is_string($relationship)) {
      $firstRelationshipNameIndex = strpos($relationship, '.');
    }

    if (empty($firstRelationshipNameIndex)) {
      if (!$relationship instanceof Relationship) {
        $relationship = static::getRelationship($relationship);
      }

      if ($relationship instanceof FieldRelationship) {
        if ($relationship->isMultiple) {
          if (!array_key_exists($relationship->getName(), $this->relatedViaFieldOnEntity)) {
            $this->createNewCollection($relationship);
          }

          return $this->relatedViaFieldOnEntity[$relationship->getName()];
        } else {
          if (array_key_exists($relationship->getName(), $this->relatedViaFieldOnEntity)) {
            return $this->relatedViaFieldOnEntity[$relationship->getName()];
          } else {
            return NULL;
          }
        }
      } else {
        if ($relationship instanceof ReferencedRelationship) {
          if (!array_key_exists($relationship->getName(), $this->relatedViaFieldOnExternalEntity)) {
            $this->createNewCollection($relationship);
          }

          return $this->relatedViaFieldOnExternalEntity[$relationship->getName()];
        }
      }
    } else {
      $firstRelationshipName = substr($relationship, 0, $firstRelationshipNameIndex);
      $firstRelationshipGet = $this->get($firstRelationshipName);

      if (!empty($firstRelationshipGet)) {
        $newRelationshipName = substr($relationship, $firstRelationshipNameIndex + 1);
        return $firstRelationshipGet->get($newRelationshipName);
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getId()
  {
    return $this->entity->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getUUID(): string
  {
    return $this->entity->{'uuid'}->value;
  }

  /**
   * Set the ID of the Model
   *
   * @param int|string|null $id
   *
   * @return void
   */
  public function setId($id)
  {
    $idField = Drupal::service('spectrum.model')->getIdField(static::class);
    $this->entity->$idField->value = $id;
  }

  /**
   * Returns the id of the provided Relationship
   *
   * @param FieldRelationship $relationship
   *
   * @return int|string|int[]|string[]|null
   */
  public function getFieldId(FieldRelationship $relationship)
  {
    $entity = $this->entity;
    $field = $relationship->getField();
    $column = $relationship->getColumn();

    if ($relationship->isSingle) // meaning the field can only contain 1 reference
    {
      return empty($entity->$field->$column) ? NULL : $entity->$field->$column;
    } else {
      $returnValue = [];
      foreach ($entity->$field->getValue() as $fieldValue) {
        $returnValue[] = $fieldValue[$column];
      }

      return $returnValue;
    }
  }

  /**
   * Create a Collection for a Relationship. Mind you this can be both Field
   * and ReferencedRelationships, ReferencedRelationships will always be a
   * Collection FieldRelationships will only be a Collection if Multiple values
   * can be filled in (fieldCardinality > 1)
   *
   * @param Relationship $relationship
   *
   * @return CollectionInterface
   */
  private function createNewCollection(Relationship $relationship): CollectionInterface
  {
    $collection = NULL;
    if ($relationship instanceof FieldRelationship) {
      if ($relationship->isMultiple) {
        if ($relationship->isPolymorphic) {
          $this->relatedViaFieldOnEntity[$relationship->getName()] = PolymorphicCollection::forgeNew(NULL);
        } else {
          $this->relatedViaFieldOnEntity[$relationship->getName()] = Collection::forgeNew($relationship->getModelType());
        }

        $collection = $this->relatedViaFieldOnEntity[$relationship->getName()];
      } else {
        throw new InvalidTypeException('Single Field Relationships do not require a collection');
      }
    } else {
      if ($relationship instanceof ReferencedRelationship) {
        $this->relatedViaFieldOnExternalEntity[$relationship->getName()] = Collection::forgeNew($relationship->getModelType());
        $collection = $this->relatedViaFieldOnExternalEntity[$relationship->getName()];
      }
    }
    return $collection;
  }

  /**
   * Set the Inverse values of this Model on the provided Relationship
   *
   * @param FieldRelationship $relationship
   * @param ModelInterface $referencedModel
   *
   * @return self
   */
  private function putInverse(FieldRelationship $fieldRelationship, ModelInterface $referencedModel): self
  {
    // if the relationship is polymorphic we can get multiple bundles, so we must define the modeltype based on the bundle and entity of the current looping entity
    // or if the related modeltype isn't set yet, we must set it once
    $referencedEntity = $referencedModel->getEntity();
    $referencedEntityType = $referencedEntity->getEntityTypeId();
    $referencedEntityBundle = $referencedEntity->bundle();

    $referencedModelType = static::service()
      ->getModelClassForEntityAndBundle($referencedEntityType, $referencedEntityBundle);

    // we must also check for an inverse relationship and, if found, put the inverse as well
    $referencedRelationship = $referencedModelType::getReferencedRelationshipForFieldRelationship($fieldRelationship);

    // And finally if we found an inverse relationship, lets put (this) on the inverse (defining an inverse is optional, so we can just as easily find no inverses)
    if (!empty($referencedRelationship)) {
      $referencedModel->put($referencedRelationship, $this, TRUE);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public final function put($relationship, $objectToPut, $includeInOriginalModels = FALSE)
  {
    $returnValue = NULL; // we return the object where this object is being put ON (can be a Model, or a Collection)

    if ($objectToPut != NULL && ($objectToPut instanceof ModelInterface || $objectToPut instanceof CollectionInterface)) {
      if (is_string($relationship)) // we only have the relationship name
      {
        $relationship = static::getRelationship($relationship);
      }

      if ($relationship instanceof FieldRelationship) {
        /** @var FieldRelationship $relationship */
        $relationshipField = $relationship->getField();
        $relationshipColumn = $relationship->getColumn();

        if ($relationship->isMultiple) {
          // In case we have a collection we want to put, lets loop over de models, and add them model per model
          if ($objectToPut instanceof CollectionInterface) {
            foreach ($objectToPut as $model) {
              $returnValue = $this->put($relationship, $model, $includeInOriginalModels);
            }
          } else {
            if ($objectToPut instanceof ModelInterface) {
              /** @var ModelInterface $objectToPut */
              // In case we have a model, it means we have to add it to the collection, that potentially doesn't exist yet
              // lets watch out, that the relationship can be polymorphic to create the correct collection if needed
              if (!array_key_exists($relationship->getName(), $this->relatedViaFieldOnEntity)) {
                $this->createNewCollection($relationship);
              }

              // we put the model on the collection
              $collection = $this->relatedViaFieldOnEntity[$relationship->getName()];
              $collection->put($objectToPut);

              if ($includeInOriginalModels) {
                $collection->putOriginal($objectToPut);
              }

              $returnValue = $collection;

              // and also append the entity field with the value (append because there can be multiple items)
              $objectToPutId = $objectToPut->getId();

              if (!empty($objectToPutId)) {
                // We need to make sure the value isnt already added to the multi entity reference field, before appending the new item

                /** @var EntityReferenceFieldItemList $fieldItemList */
                $fieldItemList = $this->entity->{$relationshipField};
                $valueExists = FALSE;

                /** @var EntityReferenceItem $fieldValue */
                foreach ($fieldItemList as $fieldValue) {
                  if ($fieldValue->$relationshipColumn == $objectToPutId) {
                    $valueExists = TRUE;
                    break;
                  }
                }

                if (!$valueExists) {
                  // The value does not yet exist on the entity, we can safely apply it
                  $this->entity->$relationshipField->appendItem($objectToPut->entity);
                }
              }

              // and finally set a possible inverse as well
              $this->putInverse($relationship, $objectToPut);
            }
          }
        } else {
          if ($relationship->isSingle) {
            // when the relationship is single (meaning only 1 reference allowed)
            // things get much easier. Namely we just put the model in the related array
            // even if the relationship is polymorphic it doesn't matter.
            $this->relatedViaFieldOnEntity[$relationship->getName()] = $objectToPut;
            $returnValue = $objectToPut;
            // we also set the new id on the current entity
            $objectToPutId = $objectToPut->getId();
            if (!empty($objectToPutId)) {
              $this->entity->$relationshipField->$relationshipColumn = $objectToPutId;
            }

            // and finally set a possible inverse as well
            $this->putInverse($relationship, $objectToPut);
          }
        }
      } else {
        if ($relationship instanceof ReferencedRelationship) {
          /** @var ReferencedRelationship $relationship */
          if (!array_key_exists($relationship->getName(), $this->relatedViaFieldOnExternalEntity)) {
            $this->createNewCollection($relationship);
          }

          $collection = $this->relatedViaFieldOnExternalEntity[$relationship->getName()];
          $collection->put($objectToPut);

          $currentId = $this->getId();
          if (!empty($currentId)) {
            $fieldRelationship = $relationship->getFieldRelationship();
            $fieldRelationshipField = $fieldRelationship->getField();
            $fieldRelationshipColumn = $fieldRelationship->getColumn();

            if ($fieldRelationship->isMultiple) {
              // We know multiple values can exist. Lets see if the ID already exists
              // Because we want to avoid setting the value multiple times

              $fieldValues = $objectToPut->entity->$fieldRelationshipField;
              $valueExists = FALSE;

              /** @var EntityReferenceItem $fieldValue */
              foreach ($fieldValues as $fieldValue) {
                if ($fieldValue->$fieldRelationshipColumn == $currentId) {
                  $valueExists = TRUE;
                  break;
                }
              }

              if (!$valueExists) {
                $objectToPut->entity->$fieldRelationshipField->appendItem([
                  $fieldRelationshipColumn => $currentId,
                ]);
              }
            } else {
              $objectToPut->entity->$fieldRelationshipField->$fieldRelationshipColumn = $currentId;
            }
          }

          if ($includeInOriginalModels) {
            $collection->putOriginal($objectToPut);
          }

          $returnValue = $collection;
        }
      }
    } else {
      if ($objectToPut === NULL) {
        if (is_string($relationship)) // we only have the relationship name
        {
          $relationship = static::getRelationship($relationship);
        }

        if ($relationship instanceof FieldRelationship) {
          $relationshipField = $relationship->getField();
          $relationshipColumn = $relationship->getColumn();

          if ($relationship->isMultiple) {
            $this->entity->$relationshipField = [];
          } else {
            if ($relationship->isSingle) {
              $this->entity->$relationshipField->$relationshipColumn = NULL;
            }
          }

          $returnValue = $objectToPut;
        }
      }
    }

    return $returnValue;
  }

  /**
   * Put a new Model on the provided Relationship
   * If relationship is referenced relationship save static first
   *
   * @param string|FieldRelationship|ReferencedRelationship $relationship
   *
   * @return ModelInterface
   */
  public final function putNew($relationship): ModelInterface
  {
    if (is_string($relationship)) // we only have the relationship name
    {
      $relationship = static::getRelationship($relationship);
    }

    $relationshipModel = NULL;
    if ($relationship instanceof FieldRelationship) {
      if ($relationship->isPolymorphic) {
        throw new PolymorphicException('PutNew has no meaning for a polymorphic relationship, we can\'t know the type');
      }

      $relationshipModelType = $relationship->getModelType();
      $relationshipModel = $relationshipModelType::forgeNew();
      $this->put($relationship, $relationshipModel);
    } else {
      if ($relationship instanceof ReferencedRelationship) {
        $id = $this->getId();
        $relationshipModelType = $relationship->getModelType();
        $relationshipModel = $relationshipModelType::forgeNew();

        // If this record already has an Id, we can fill it in on the new model
        if (!empty($id)) {
          $relationshipField = $relationship->fieldRelationship->getField();
          $relationshipColumn = $relationship->fieldRelationship->getColumn();

          $relationshipModel->entity->$relationshipField->$relationshipColumn = $id;
        }

        // We put it on the inverse, that way the ->putInverse() is triggered and put on $this as well
        $relationshipModel->put($relationship->fieldRelationship, $this);
      }
    }
    return $relationshipModel;
  }

  /**
   * {@inheritdoc}
   */
  public function constraints() {}

  /**
   * Add a FieldConstraint to the Drupal Entity at Runtime
   *
   * @param string $fieldName
   * @param string $constraintName
   * @param array $options
   *
   * @return self
   */
  public function addFieldConstraint(string $fieldName, string $constraintName, array $options = []): self
  {
    /** @var FieldableEntityInterface $entity */
    $entity = $this->getEntity();
    $entity->getFieldDefinition($fieldName)
      ->addConstraint($constraintName, $options);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCopiedModel(): ModelInterface
  {
    $copy = static::forgeByEntity($this->getCopiedEntity());
    return $copy;
  }

  /**
   * Create a Copy of this entity, the IDs will be blank, and all the field
   * values will be filled in.
   *
   * @return EntityInterface
   */
  public function getCopiedEntity(): EntityInterface
  {
    $entity = $this->entity;
    $copy = $entity->createDuplicate();

    return $copy;
  }

  /**
   * Create a Clone of the Entity, and wrap it in a Model, with all the fields
   * including the IDs filled in.
   *
   * @return ModelInterface
   */
  public function getClonedModel(): ModelInterface
  {
    $clone = static::forgeByEntity($this->getClonedEntity());
    return $clone;
  }

  /**
   * Create a Clone of this Entity, with all the fields including the IDs
   * filled in.
   *
   * @return EntityInterface
   */
  public function getClonedEntity(): EntityInterface
  {
    $entity = $this->entity;
    $clone = $entity->createDuplicate();

    $idField = Drupal::service('spectrum.model')->getIdField(static::class);
    $clone->$idField->value = $this->getId();

    return $clone;
  }

  /**
   * PRIVATE API. DO NOT USE!
   * Sets the isNewlyInserted flag
   *
   * @param boolean $value
   *
   * @return self
   */
  public function __setIsNewlyInserted(bool $value): self
  {
    $this->__isNewlyInserted = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isNewlyInserted(): bool
  {
    return $this->__isNewlyInserted;
  }

  /**
   * PRIVATE API. DO NOT USE!
   * Sets the isBeingDeleted flag
   *
   * @param boolean $value
   *
   * @return self
   * @internal
   *
   */
  public function __setIsBeingDeleted(bool $value): self
  {
    $this->__isBeingDeleted = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isBeingDeleted(): bool
  {
    return $this->__isBeingDeleted;
  }

  /**
   * This method checks whether a field changed from an old value to something
   * else, it is especially useful in triggermethods to determine if certain
   * logic should be executed
   *
   * @param string $fieldName the fieldname you want to check (for example
   *   field_body)
   * @param mixed $oldValue what the old value should be, to return true for
   *
   * @return boolean
   */
  public final function fieldChangedFrom(string $fieldName, $oldValue): bool
  {
    $returnValue = FALSE;

    // If this is a new value, the old value can only be null else return false
    if ($this->isNewlyInserted()) {
      return ($oldValue === NULL);
    }

    // Next we check if the field actually changed
    if ($this->fieldChanged($fieldName)) {
      // Now we know the field changed, lets compare it to the oldvalue
      $fieldDefinition = static::service()
        ->getFieldDefinition(static::class, $fieldName);
      $oldAttribute = $this->entity->original->$fieldName;

      switch ($fieldDefinition->getType()) {
        case 'address':
        case 'geolocation':
          throw new InvalidFieldException('Field type isnt supported for fieldChangedFrom functions, only single value fields supported');
          break;
        case 'entity_reference':
        case 'entity_reference_revisions':
        case 'file':
        case 'image':
          $returnValue = ($oldAttribute->target_id == $oldValue);
          break;
        case 'link':
          $returnValue = ($oldAttribute->uri == $oldValue);
          break;
        default:
          $returnValue = ($oldAttribute->value == $oldValue);
          break;
      }
    }

    return $returnValue;
  }

  /**
   * This method checks whether a field changed from an old value to a specific
   * new value, It is especially useful in triggermethods to determine, if
   * certain logic should be executed
   *
   * @param string $fieldName the fieldname you want to check (for example
   *   field_body)
   * @param mixed $oldValue what the old value should be
   * @param mixed $newValue what the old value should be
   *
   * @return boolean
   */
  public final function fieldChangedFromTo(string $fieldName, $oldValue, $newValue): bool
  {
    $returnValue = FALSE;
    $isNew = $this->isNewlyInserted();

    // Next we check if the field actually changed
    if ($this->fieldChanged($fieldName)) {
      // Now we know the field changed, lets compare it to the oldvalue
      $fieldDefinition = static::service()
        ->getFieldDefinition(static::class, $fieldName);
      $newAttribute = $this->entity->$fieldName;
      $oldAttribute = isset($this->entity->original) ? $this->entity->original->$fieldName : NULL;

      switch ($fieldDefinition->getType()) {
        case 'address':
        case 'geolocation':
          throw new InvalidFieldException('Field type isnt supported for fieldChangedFrom functions, only single value fields supported');
          break;
        case 'entity_reference':
        case 'entity_reference_revisions':
        case 'file':
        case 'image':
          $returnValue = (($isNew && $oldValue == NULL) || (!$isNew && $oldAttribute->target_id == $oldValue)) && ($newAttribute->target_id == $newValue);
          break;
        case 'link':
          $returnValue = (($isNew && $oldValue == NULL) || (!$isNew && $oldAttribute->uri == $oldValue)) && ($newAttribute->uri == $newValue);
          break;
        default:
          $returnValue = (($isNew && $oldValue == NULL) || (!$isNew && $oldAttribute->value == $oldValue)) && ($newAttribute->value == $newValue);
          break;
      }
    }

    return $returnValue;
  }

  /**
   * This method checks whether a field changed to a certain new value
   * (independently of what it was before, as long as it actually changed) It
   * is especially useful in triggermethods to determine  if certain logic
   * should be executed
   *
   * @param string $fieldName the fieldname you want to check (for example
   *   field_body)
   * @param $newValue
   *
   * @return boolean
   * @throws \Drupal\spectrum\Exceptions\InvalidFieldException
   */
  public final function fieldChangedTo(string $fieldName, $newValue): bool
  {
    $returnValue = FALSE;
    // Next we check if the field actually changed
    if ($this->fieldChanged($fieldName)) {
      // Now we know the field changed, lets compare it to the new value
      $fieldDefinition = static::service()
        ->getFieldDefinition(static::class, $fieldName);
      $newAttribute = $this->entity->$fieldName;

      switch ($fieldDefinition->getType()) {
        case 'address':
        case 'geolocation':
          throw new InvalidFieldException('Field type isnt supported for fieldChangedTo functions, only single value fields supported');
          break;
        case 'entity_reference':
        case 'entity_reference_revisions':
        case 'file':
        case 'image':
          $returnValue = ($newAttribute->target_id == $newValue);
          break;
        case 'link':
          $returnValue = ($newAttribute->uri == $newValue);
          break;
        default:
          $returnValue = ($newAttribute->value == $newValue);
          break;
      }
    }

    return $returnValue;
  }

  /**
   * Returns TRUE if any of the fields provided in the array changed value
   *
   * @param string[] $fieldNames
   * @param boolean $ignoreFieldDoesNotExist
   *
   * @return boolean
   */
  public final function someFieldsChanged(array $fieldNames, bool $ignoreFieldDoesNotExist = FALSE): bool
  {
    $returnValue = FALSE;

    foreach ($fieldNames as $fieldName) {
      if ($this->fieldChanged($fieldName, $ignoreFieldDoesNotExist)) {
        $returnValue = TRUE;
        break;
      }
    }

    return $returnValue;
  }

  /**
   * Helper function for trigger methods. Returns true if the value of the
   * field changed compared to the value stored in the database This can be
   * used to only execute certain code when a field changes. (For Example when
   * setting the Title of a User based on the first and lastname, only execute
   * the method when the first of the lastname changes)
   *
   * @param string $fieldName
   * @param bool $ignoreFieldDoesNotExist
   *
   * @return bool
   */
  public final function fieldChanged(string $fieldName, bool $ignoreFieldDoesNotExist = FALSE): bool
  {
    $returnValue = $this->isNewlyInserted();

    if ($returnValue) {
      return TRUE;
    }

    if (!isset($this->entity->original)) {
      // Original isnt set on insert and delete. On insert function will return TRUE (see above)
      // On delete this should return false.
      return FALSE;
    }

    $fieldDefinition = static::service()
      ->getFieldDefinition(static::class, $fieldName);

    if ($ignoreFieldDoesNotExist && empty($fieldDefinition)) {
      return FALSE;
    }

    if (empty($fieldDefinition)) {
      throw new Error('Field definition for "' . $fieldName . '" not found, field does not exists?');
    }

    $newAttribute = $this->entity->$fieldName;
    $oldAttribute = $this->entity->original->$fieldName;

    switch ($fieldDefinition->getType()) {
      case 'address':
        $returnValue = ($newAttribute->country_code != $oldAttribute->country_code)
          || ($newAttribute->country_code != $oldAttribute->country_code)
          || ($newAttribute->administrative_area != $oldAttribute->administrative_area)
          || ($newAttribute->locality != $oldAttribute->locality)
          || ($newAttribute->postal_code != $oldAttribute->postal_code)
          || ($newAttribute->sorting_code != $oldAttribute->sorting_code)
          || ($newAttribute->address_line1 != $oldAttribute->address_line1)
          || ($newAttribute->address_line2 != $oldAttribute->address_line2);
        break;
      case 'entity_reference':
      case 'entity_reference_revisions':
      case 'file':
      case 'image':
        if (
          $fieldDefinition->getFieldStorageDefinition()
          ->getCardinality() === -1
        ) {
          // check order of target_ids in newAttribute and oldAttribute
          if (count($newAttribute->getValue()) === 0 && count($oldAttribute->getValue())) {
            return FALSE;
          } else {
            if ((count($newAttribute->getValue()) === count($oldAttribute->getValue()))) {
              $oldIterator = new ArrayIterator($oldAttribute->getValue());
              foreach ($newAttribute->getValue() as $value) {
                if ($value !== $oldIterator->current()) {
                  $returnValue = TRUE;
                  break;
                } else {
                  $returnValue = FALSE;
                }
                $oldIterator->next();
              }
            } else {
              $returnValue = TRUE;
            }
          }
        } else {
          $returnValue = ($newAttribute->target_id != $oldAttribute->target_id);
        }
        break;
      case 'geolocation':
        $returnValue = ($newAttribute->lag != $oldAttribute->lag) || ($newAttribute->lng != $oldAttribute->lng);
        break;
      case 'link':
        $returnValue = ($newAttribute->uri != $oldAttribute->uri);
        break;
      default:
        if ($newAttribute->value === 0 || $oldAttribute->value === 0) {
          $returnValue = ($newAttribute->value !== $oldAttribute->value);
        } else {
          $returnValue = ($newAttribute->value != $oldAttribute->value);
        }
        break;
    }

    return $returnValue;
  }

  /**
   * {@inheritdoc}
   */
  public final function refresh(?string $relationshipName = NULL): self
  {
    if ($relationshipName === NULL) {
      if ($this->isNew()) {
        throw new ModelNotFoundException('You cant refresh a model which doesnt exist in the database');
      }

      // Drupal caches the entities in memory for the remainder of the transaction
      // we want to clear that cash, because we want the data as it is in the database
      $modelClass = get_called_class();
      static::service()->clearDrupalEntityCacheForModelClass($modelClass);

      $idField = Drupal::service('spectrum.model')->getIdField(static::class);
      // We do a new entity query
      $entityQuery = static::getEntityQuery();
      $entityQuery->addCondition(new Condition($idField, '=', $this->getId()));

      $entity = $entityQuery->fetchSingle();

      if (empty($entity)) {
        throw new ModelNotFoundException('Model no longer exists in the database');
      }

      // And replace the entity in this model, with the queried entity
      $this->entity = $entity;
    } else {
      // A relationship is passed, we get it, and call the refresh on that instead.
      if ($subRefresh = $this->get($relationshipName)) {
        $subRefresh->refresh();
      }
    }

    return $this;
  }

  /**
   * Checks if the provided Relationship Name exists as a Relationship
   *
   * @param string $relationshipName
   *
   * @return boolean
   */
  public static function hasRelationship(string $relationshipName): bool
  {
    $sourceModelType = get_called_class();
    static::setRelationships($sourceModelType);
    return array_key_exists($sourceModelType, static::$relationships) && array_key_exists($relationshipName, static::$relationships[$sourceModelType]);
  }

  /**
   * Identical to hasRelationship, but with the difference that we search for
   * deep relationships via the '.'
   *
   * @param string $relationshipName
   *
   * @return boolean
   */
  public static function hasDeepRelationship(string $relationshipName): bool
  {
    $firstRelationshipNamePosition = strpos($relationshipName, '.');

    if (empty($firstRelationshipNamePosition)) // relationship name without extra relationships
    {
      return static::hasRelationship($relationshipName);
    }

    $firstRelationshipName = substr($relationshipName, 0, $firstRelationshipNamePosition);
    if (static::hasRelationship($firstRelationshipName)) {
      $firstRelationship = static::getRelationship($firstRelationshipName);

      if ($firstRelationship instanceof FieldRelationship) {
        /** @var FieldRelationship $firstRelationship */
        if ($firstRelationship->isPolymorphic) {
          $hasDeepRelationship = TRUE;
          foreach ($firstRelationship->getPolymorphicModelTypes() as $polymorphicModelType) {
            $hasDeepRelationship = $polymorphicModelType::hasDeepRelationship(substr($relationshipName, $firstRelationshipNamePosition + 1));

            if (!$hasDeepRelationship) {
              break;
            }
          }

          return $hasDeepRelationship;
        }

        $firstRelationshipModelType = $firstRelationship->getModelType();
        return $firstRelationshipModelType::hasDeepRelationship(substr($relationshipName, $firstRelationshipNamePosition + 1));
      }

      $firstRelationshipModelType = $firstRelationship->getModelType();
      return $firstRelationshipModelType::hasDeepRelationship(substr($relationshipName, $firstRelationshipNamePosition + 1));
    }

    return FALSE;
  }

  /**
   * Returns a Relationship based on the relationshipName, this will also look
   * for relationships when a "." so can be used to search multiple levels deep
   *
   * @param string $relationshipName
   *
   * @return Relationship
   */
  public static function getDeepRelationship(string $relationshipName): Relationship
  {
    $firstRelationshipNamePosition = strpos($relationshipName, '.');

    // relationship name without extra relationships
    if (empty($firstRelationshipNamePosition)) {
      return static::getRelationship($relationshipName);
    }

    $firstRelationshipName = substr($relationshipName, 0, $firstRelationshipNamePosition);
    $nextRelationshipNames = substr($relationshipName, $firstRelationshipNamePosition + 1, strlen($relationshipName));
    $firstRelationship = static::getRelationship($firstRelationshipName);

    if ($firstRelationship instanceof FieldRelationship) {
      /** @var FieldRelationship $firstRelationship */
      if ($firstRelationship->isPolymorphic) {
        $firstRelationshipModelType = $firstRelationship->getPolymorphicModelTypes()[0];
      } else {
        $firstRelationshipModelType = $firstRelationship->getModelType();
      }
    } else {
      $firstRelationshipModelType = $firstRelationship->getModelType();
    }

    return $firstRelationshipModelType::getDeepRelationship($nextRelationshipNames);
  }

  /**
   * Generate a new Key in memory, used to generate a temporary key for a model
   * that doesnt exists in the db yet, and the ID cant be used
   *
   * @return string
   */
  public static function getNextKey(): string
  {
    return 'PLH' . (static::$keyIndex++);
  }

  /**
   * Create a new entity and wrap it in this Model
   *
   * @return static
   */
  public static function forgeNew(): static
  {
    $store = Drupal::entityTypeManager()->getStorage(static::entityType());

    if (!empty(static::bundle())) {
      $entity = $store->create(['type' => static::bundle()]);
    } else {
      $entity = $store->create();
    }

    return static::forgeByEntity($entity);
  }

  /**
   * Forge a Model by a Drupal Entity (no queries will be done)
   *
   * @param EntityInterface $entity
   *
   * @return static|null
   */
  public static function forgeByEntity(EntityInterface $entity, string $class = NULL): ?static
  {
    return static::forge($entity, NULL, $class);
  }

  /**
   * Forge a Model by the provided ID, a query will be done to the database
   *
   * @param int|string $id
   *
   * @return static|null
   */
  public static function forgeById(string|int $id, bool $useAccessPolicy = false): ?static
  {
    return static::forge(entity: NULL, id: $id, class: static::class,useAccessPolicy: $useAccessPolicy);
  }

  /**
   * Forge a new Model with either an Drupal Entity or an ID. For ease of use
   * and readability use the methods "forgeById" or "forgeByEntity" This is
   * only used internally
   *
   * @param EntityInterface $entity
   * @param string|int|null $id
   *
   * @return ModelInterface|null will return the Model, or null if the model
   *   wasnt found
   */
  private static function forge(
    EntityInterface $entity = NULL,
    string|int|null $id = NULL,
    string $class = NULL,
    bool $useAccessPolicy = false
  ): ?static {
    if (!empty($id)) {
      $query = static::getEntityQuery();

      $idField = static::service()->getIdField(static::class);
      // add a condition on the id
      $query->addCondition(new Condition($idField, '=', $id));

      if ($bundle = static::bundle()) {
        $query->addCondition(new Condition('type', '=', $bundle));
      }

      if($useAccessPolicy){
          $query->setAccessPolicy(static::getAccessPolicy());
      }

      return $query->fetchSingleModel($class);
    }

    if ($entity === NULL) {
      return static::forgeNew();
    }

    if ($class !== NULL) {
      return $class::forgeByEntity($entity);
    }

    $registeredModelType = static::service()->getModelClassForEntity($entity);
    $requestedModelType = static::class;

    if (is_subclass_of($requestedModelType, $registeredModelType)) {
      // When the requestedmodeltype is a subclass of the registeredmodeltype, we use the requestedmodeltype
      // As it might just as well be a seperate implementation for another purpose.
      // This is needed for QueuedJobs, as they all implement logic from QueuedJob, which is registered in the system.
      // And all queuedjobs extend from QueuedJob, but arent registered in the modelservice.
      return new $requestedModelType($entity);
    }

    if ($requestedModelType === File::class && $registeredModelType === Image::class) {
      // A special case, because Image extends from File, and File is registered in the modelservice
      return new $requestedModelType($entity);
    }

    // If the requestedmodeltype is not a subclass of the registered modeltype,
    // We use the registeredmodeltype, as it should be the other way round.
    return new $registeredModelType($entity);
  }

  /**
   * Return a ModelQuery for the current Model (with the correct entity and
   * bundle filled in as Conditions)
   *
   * @return ModelQuery
   */
  public static function getModelQuery(): ModelQuery
  {
    return new ModelQuery(get_called_class());
  }

  /**
   * Return an EntityQuery, (with the correct entity filled in as a Condition)
   *
   * @return EntityQuery
   */
  public static function getEntityQuery(): EntityQuery
  {
    return new EntityQuery(static::entityType());
  }

  /**
   * Return a BundleQuery with the entityType and bundle filled in as Conditions
   *
   * @return BundleQuery
   */
  public static function getBundleQuery(): BundleQuery
  {
    return new BundleQuery(static::entityType(), static::bundle());
  }

  /**
   * Return the ReferencedRelationship for a FieldRelationship (the inverse on
   * the other Model), can be null if the ReferencedRelationship isnt defined
   *
   * @param FieldRelationship $fieldRelationship
   *
   * @return ReferencedRelationship|null
   */
  public static function getReferencedRelationshipForFieldRelationship(FieldRelationship $fieldRelationship): ?ReferencedRelationship
  {
    $relationships = static::getRelationships();
    $referencedRelationship = NULL;
    foreach ($relationships as $relationship) {
      if ($relationship instanceof ReferencedRelationship) {
        if ($relationship->fieldRelationship === $fieldRelationship) {
          $referencedRelationship = $relationship;
          break;
        }
      }
    }

    return $referencedRelationship;
  }

  /**
   * This method is used to add relationships on every implementation of a Model
   *
   * @return Relationship[]
   */
  public static function relationships()
  {
    return [];
  }

  /**
   * This Method sets the relationship arrays (and takes care of the sharing of
   * the scope of Model by multiple Models)
   *
   * @param [type] $modelType
   *
   * @return void
   */
  public static function setRelationships($modelType)
  {
    if (!array_key_exists($modelType, static::$relationships)) {
      static::$relationships[$modelType] = [];
      static::relationships();
    }
  }

  /**
   * Get a Relationship by Name
   *
   * @param string $relationshipName
   *
   * @return Relationship
   */
  public static function getRelationship(string $relationshipName): Relationship
  {
    $sourceModelType = get_called_class();
    static::setRelationships($sourceModelType);

    if ($sourceModelType::hasRelationship($relationshipName)) {
      return static::$relationships[$sourceModelType][$relationshipName];
    }

    throw new RelationshipNotDefinedException('Relationship "' . $relationshipName . '" does not exist on model "' . $sourceModelType . '"');
  }

  /**
   * Get a relationship by the fieldname on the drupal entity (used for
   * deserializing)
   *
   * @param string $fieldName
   *
   * @return void
   */
  public static function getRelationshipByFieldName(string $fieldName): ?FieldRelationship
  {
    $relationships = static::getRelationships();
    $foundRelationship = NULL;

    foreach ($relationships as $relationship) {
      if ($relationship instanceof FieldRelationship && $relationship->getField() === $fieldName) {
        $foundRelationship = $relationship;
        break;
      }
    }

    return $foundRelationship;
  }

  /**
   * Returns an array with all the relationships of the current Model
   *
   * @return Relationship[]
   */
  public static function getRelationships(): array
  {
    $sourceModelType = get_called_class();
    static::setRelationships($sourceModelType);

    return static::$relationships[$sourceModelType];
  }

  /**
   * Add a relationship to the Model, this function should be used in the
   * relationships() function on every implementation of Model
   *
   * @param Relationship $relationship
   *
   * @return void
   */
  public static function addRelationship(Relationship $relationship)
  {
    // first we need to namespace the relationships, as the relationship array is staticly defined;
    // meaning if we would add 2 relationships with the same name on different models, the first one would be overridden
    // we use the relationshipKey, which is a namespaced version with the relationship source added
    $sourceModelType = get_called_class();
    if (!array_key_exists($sourceModelType, static::$relationships)) {
      static::$relationships[$sourceModelType] = [];
    }

    $relationship->setRelationshipSource($sourceModelType);
    static::$relationships[$sourceModelType][$relationship->getName()] = $relationship;
  }

  /**
   * Safe check to see if a getterMethod exists on the model. This shields any
   * platform functions and only checks for functions on the lowest level of
   * abstraccion This is used by SimpleModelWrapper
   *
   * @param ModelInterface $model
   * @param string $property
   *
   * @return boolean
   */
  public static function getterExists(ModelInterface $model, string $property): bool
  {
    $getterExists = FALSE;

    $getterName = 'get' . ucfirst($property);
    if (!empty($property) && is_callable([$model, $getterName])) {
      if ($getterName === 'getShortModelName') {
        $getterExists = TRUE;
      } else {
        $reflector = new ReflectionMethod($model, $getterName);
        $isProto = ($reflector->getDeclaringClass()
          ->getName() !== get_class($model));

        $getterExists = !$isProto; // Meaning the function may only exist on the child class (shielding Model class functions from this)
      }
    }

    return $getterExists;
  }

  /**
   * Call a getter function on the model. This is used by SimpleModelWrapper,
   * in combination with getterExists to safely call getters on the lowest
   * level of abstraction (the most child method)
   *
   * @param string $property
   */
  public final function callGetter(string $property)
  {
    $getterName = 'get' . $property;
    return $this->$getterName();
  }

  /**
   * Magic getter implementation of Model, this checks whether the property
   * exists or the relationship,
   *
   * @param string $property
   *
   * @return void
   */
  public function __get($property)
  {
    if (property_exists($this, $property)) {
      return $this->$property;
    }

    // lets check for pseudo properties
    if (array_key_exists($property, $this->relatedViaFieldOnEntity)) {
      return $this->relatedViaFieldOnEntity[$property];
    }

    // lets check for pseudo properties
    if (array_key_exists($property, $this->relatedViaFieldOnExternalEntity)) {
      return $this->relatedViaFieldOnExternalEntity[$property];
    }

    if (static::hasRelationship($property)) {
      return $this->get($property);
    }
  }

  /**
   * used in combination with the magic getter to get values dynamically by
   * twig templates
   *
   * @param string $property
   *
   * @return boolean
   */
  public function __isset($property)
  {
    // Needed for twig to be able to access relationship via magic getter
    return property_exists($this, $property) || array_key_exists($property, $this->relatedViaFieldOnEntity) || array_key_exists($property, $this->relatedViaFieldOnExternalEntity) || static::hasRelationship($property);
  }

  /**
   * {@inheritdoc}
   */
  public function beforeValidate() {}

  /**
   * {@inheritdoc}
   */
  public function beforeInsert() {}

  /**
   * {@inheritdoc}
   */
  public function afterInsert() {}

  /**
   * {@inheritdoc}
   */
  public function beforeUpdate() {}

  /**
   * {@inheritdoc}
   */
  public function afterUpdate() {}

  /**
   * {@inheritdoc}
   */
  public function beforeDelete() {}

  /**
   * {@inheritdoc}
   */
  public function afterDelete() {}

  /**
   * This method will automatically do cascading deletes for relationships
   * (both Field and ReferencedRelationships) that have the cascading defined
   * in the Relationship
   *
   * @return void
   * @throws CascadeNoDeleteException
   */
  public final function doCascadingDeletes()
  {
    $relationships = static::getRelationships();
    foreach ($relationships as $relationship) {
      if ($relationship->isCascadeOnDelete()) {
        $fetchedRelationship = $this->fetch($relationship->getName());
        if (!empty($fetchedRelationship)) {
          if ($fetchedRelationship instanceof CollectionInterface) {
            foreach ($fetchedRelationship as $model) {
              $model->delete();
            }
          } else {
            if ($fetchedRelationship instanceof ModelInterface) {
              $fetchedRelationship->delete();
            }
          }
        }
      } else {
        if ($relationship->isCascadeNoDelete()) {
          $fetchedRelationship = $this->fetch($relationship->getName());
          if (isset($fetchedRelationship) && !getenv('IGNORE_DELETE_SAFETY')) {
            throw new CascadeNoDeleteException('Trying to delete "' . Drupal::service('spectrum.model')
              ->getBundleKey(static::class) . '" when there are "' . $relationship->getName() . '" present.');
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadTranslation(array $languageCodes): self
  {
    $entity = $this->entity;

    if ($entity instanceof ContentEntityInterface) {
      if (empty($languageCodes) || !$entity->isTranslatable()) {
        return $this;
      }

      foreach ($languageCodes as $languageCode) {
        if ($entity->hasTranslation($languageCode)) {
          $translatedEntity = $entity->getTranslation($languageCode);
          $this->setEntity($translatedEntity);
          break;
        }
      }
    } else {
      throw new InvalidEntityException('Translations only available on Entities that implement ContentEntityInterface');
    }

    return $this;
  }

  /**
   * Returns the Created date. Returns null if the entity is unsaved.
   *
   * @return \DateTime|null
   */
  public function getCreatedDate(): ?DateTime
  {
    $timestamp = $this->entity->{'created'}->value;
    return empty($timestamp) ? NULL : DateTime::createFromFormat('U', $timestamp);
  }

  /**
   * @param \DateTime $value
   *
   * @return self
   */
  public function setCreatedDate(DateTime $value): self
  {
    $this->entity->{'created'}->value = $value->format('U');
    return $this;
  }

  /**
   * Returns the last modified date of the entity. Returns null if the entity
   * is unsaved
   *
   * @return \DateTime|null
   */
  public function getLastModifiedDate(): ?DateTime
  {
    $timestamp = $this->entity->{'changed'}->value;
    return empty($timestamp) ? NULL : DateTime::createFromFormat('U', $timestamp);
  }

  /**
   * @param \DateTime $value
   *
   * @return self
   */
  public function setLastModifiedDate(DateTime $value): self
  {
    $this->entity->{'created'}->value = $value->format('U');
    return $this;
  }

  /**
   * Creates and sets a File on a field on a Model
   *
   * @param string $fieldName
   * @param string $filename
   * @param string $fileContent
   *
   * @return File
   */
  public function setFileField(string $fieldName, string $filename, string $fileContent): File
  {
    $fieldDefinition = Drupal::service('spectrum.model')
      ->getFieldDefinition(static::class, $fieldName);

    if (empty($fieldDefinition)) {
      throw new RuntimeException('Field not found: ' . $fieldName);
    }

    if ($fieldDefinition->getType() !== 'file') {
      throw new RuntimeException('Field type is not a file');
    }

    $fieldSettings = $fieldDefinition->getSettings();

    $file = Drupal::service('spectrum.file')->createNewFile(
      $fieldSettings['uri_scheme'],
      $fieldSettings['file_directory'],
      $filename,
      $fileContent
    );

    $this->entity->$fieldName->target_id = $file->getId();
    return $file;
  }

  /**
   * Creates and sets an Image on a field on a Model
   *
   * @param string $fieldName
   * @param string $filename
   * @param string $fileContent
   *
   * @return File
   */
  public function setImageField(string $fieldName, string $filename, string $fileContent): File
  {
    $fieldDefinition = Drupal::service('spectrum.model')
      ->getFieldDefinition(static::class, $fieldName);

    if (empty($fieldDefinition)) {
      throw new RuntimeException('Field not found: ' . $fieldName);
    }

    if ($fieldDefinition->getType() !== 'image') {
      throw new RuntimeException('Field type is not an image');
    }

    $fieldSettings = $fieldDefinition->getSettings();

    $file = Drupal::service('spectrum.file')->createNewFile(
      $fieldSettings['uri_scheme'],
      $fieldSettings['file_directory'],
      $filename,
      $fileContent
    );

    $this->entity->$fieldName->target_id = $file->getId();
    return $file;
  }

  /**
   * Returns the ModelService currently in the container,
   * This should only be used from within a Model
   *
   * @return ModelServiceInterface
   */
  private static function service(): ModelServiceInterface
  {
    if (!Drupal::hasService('spectrum.model')) {
      throw new NotImplementedException('No model service found in the Container, please create a custom module, register a service and implement \Drupal\spectrum\Services\ModelServiceInterface');
    }

    return Drupal::service('spectrum.model');
  }

  /**
   * Get the entity that was wrapped by this Model
   *
   * @return  EntityInterface
   */
  public final function getEntity(): EntityInterface
  {
    return $this->entity;
  }

  /**
   * Set the entity that this model will wrap
   *
   * @param EntityInterface $entity Your new entity
   *
   * @return  self
   */
  public final function setEntity(EntityInterface $entity): self
  {
    $this->entity = $entity;

    return $this;
  }

  /**
   * Sets the selected flag on every model of a relationship to true
   *
   * @param string $relationshipName
   *
   * @return self
   */
  public final function setRelationshipSelected(string $relationshipName): self
  {
    $valuesToSet = $this->get($relationshipName);

    if (isset($valuesToSet)) {
      if ($valuesToSet instanceof ModelInterface) {
        $valuesToSet->selected = TRUE;
      } else {
        if ($valuesToSet instanceof CollectionInterface) {
          $valuesToSet->selectAll();
        }
      }
    }

    return $this;
  }

  /**
   * Sets the selected flag on every model of a relationship to false
   *
   * @param string $relationshipName
   *
   * @return self
   */
  public final function setRelationshipDeselected(string $relationshipName): self
  {
    $valuesToSet = $this->get($relationshipName);

    if (isset($valuesToSet)) {
      if ($valuesToSet instanceof ModelInterface) {
        $valuesToSet->selected = FALSE;
      } else {
        if ($valuesToSet instanceof CollectionInterface) {
          $valuesToSet->deselectAll();
        }
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public final function isRelatedViaFieldRelationshipInMemory(string $relationshipName): bool
  {
    return array_key_exists($relationshipName, $this->relatedViaFieldOnEntity);
  }

  /**
   * {@inheritdoc}
   */
  public final function isRelatedViaReferencedRelationshipInMemory(string $relationshipName): bool
  {
    return array_key_exists($relationshipName, $this->relatedViaFieldOnExternalEntity);
  }

  /**
   * {@inheritdoc}
   */
  public final function getReferencedRelationshipsInMemory(): array
  {
    return $this->relatedViaFieldOnExternalEntity;
  }

  /**
   * {@inheritdoc}
   */
  public final function getFieldRelationshipsInMemory(): array
  {
    return $this->relatedViaFieldOnEntity;
  }

  /**
   * {@inheritdoc}
   */
  public final function getModelIdentifier()
  {
    return $this->key;
  }

  /**
   * {@inheritdoc}
   */
  public final function setModelIdentifier($identifier): self
  {
    $this->key = $identifier;
    return $this;
  }
}
