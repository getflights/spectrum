<?php

declare(strict_types=1);

namespace Drupal\spectrum\Model\Attributes;

use Attribute;
use InvalidArgumentException;
use ReflectionFunction;

#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_PARAMETER)]
final class Fetch {

  public readonly array $relationships;

  public function __construct(
    string ...$relationships
  ) {
    $this->relationships = $relationships;
  }

  public static function fromAttributes(callable $callable, mixed ...$parameters): void {
    $reflectionFunction = new ReflectionFunction($callable);

    $attributes = $reflectionFunction->getAttributes(Fetch::class);

    foreach ($attributes as $attribute) {
      /** @var \Drupal\spectrum\Model\Attributes\Fetch $instance */
      $instance = $attribute->newInstance();

      foreach ($instance->relationships as $relationship) {
        /** @var \Drupal\spectrum\Model\ModelInterface $model */
        $model = $reflectionFunction->getClosureThis();
        $model->fetch($relationship);
      }
    }

    $reflectionParameters = $reflectionFunction->getParameters();

    foreach ($reflectionParameters as $key => $reflectionParameter) {
      $attributes = $reflectionParameter->getAttributes(Fetch::class);

      if (count($attributes) === 0) {
        continue;
      }

      if (!array_key_exists($key, $parameters)) {
        throw new InvalidArgumentException('amount of parameters given does not match the amount of parameters required to call the function');
      }

      foreach ($attributes as $attribute) {
        /** @var \Drupal\spectrum\Model\Attributes\Fetch $instance */
        $instance = $attribute->newInstance();

        foreach ($instance->relationships as $relationship) {
          $parameters[$key]->fetch($relationship);
        }
      }
    }
  }

}