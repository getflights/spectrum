<?php

declare(strict_types=1);

namespace Drupal\spectrum\Model;

final class FieldDefinition {

  public function __construct(
    public readonly string $targetType,
    public readonly array $targetBundles,
    public readonly int $cardinality,
  ) {}

}