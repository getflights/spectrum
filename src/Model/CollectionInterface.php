<?php

namespace Drupal\spectrum\Model;

use Countable;
use Drupal\spectrum\Query\Query;
use IteratorAggregate;

interface CollectionInterface extends IteratorAggregate, Countable
{

  /**
   * What ModelType the Collection is created for
   *
   * @return string
   */
  public function getModelType(): string;

  /**
   * Fetch a relationshipname from the database, the data in the Collection
   * will not be cleared Instead data that is being fetched will be appeneded
   * to the Collection. If a newly fetched record already exists in the
   * collection, it will be overwritten by the new Model If you want to clear
   * data from the collection first, look at the ->clear() method.
   *
   * @param string $relationshipName
   * @param Query $queryToCopyFrom (optional) add a query to fetch, to limit
   *   the amount of results when fetching, all base conditions, conditions and
   *   conditiongroups will be add to the fetch query
   *
   * @return CollectionInterface the fetched relationship
   */
  public function fetch(string $relationshipName, ?Query $queryToCopyFrom = NULL): CollectionInterface;

  /**
   * Returns a Collection with the Models of the provided relationship
   *
   * @param string $relationshipName
   *
   * @return CollectionInterface
   */
  public function get(string $relationshipName): CollectionInterface;

  /**
   * This function saves either all the models in this collection, or if a
   * relationshipName was passed get the relationship, and perform save on that
   * relationship
   *
   * @param string $relationshipName
   *
   * @return self
   */
  public function save(string $relationshipName = NULL): self;

  /**
   * Returns the first Element in the Collection or null when the collection is
   * empty
   *
   * @return ModelInterface|null
   */
  public function first(): ?ModelInterface;

  /**
   * Returns the Model on a certain position
   *
   * @param integer $number (0 based position)
   *
   * @return ModelInterface|null
   */
  public function getModelOnPosition(int $number): ?ModelInterface;

  /**
   * This function loads the translation on all the models in this collection,
   * the first found translation will be used on the model. In case no
   * translation is found, the default language will be loaded If not all
   * models have a translation, it is possible that you get models in different
   * languages
   *
   * @param String[] $languageCodes an array containing the languagecodes you
   *   want to load on the entity
   *
   * @return self
   */
  public function loadTranslation(array $languageCodes): self;

  /**
   * Sort the collection according to a sorting function on the implemented
   * Models
   *
   * @param string|callable $sortingFunction
   *
   * @return self
   */
  public function sort($sortingFunction): self;

  /**
   * Replace the key of the model in the models and originalModels arrays with
   * a new value
   *
   * @param string|int $oldKey
   * @param string|int $newKey
   *
   * @return self
   */
  public function replaceOldModelKey($oldKey, $newKey): self;

  /**
   * Returns the models of the collection
   *
   * @return ModelInterface[]
   */
  public function getModels(): array;

  /**
   * Returns the original models of the collection
   *
   * @return ModelInterface[]
   */
  public function getOriginalModels(): array;

  /**
   * Return the models that were removed from the collection in order to delete
   * them from the database
   *
   * @return array
   */
  public function getModelsToDelete(): array;

  /**
   * Remove the Model from the Collection
   *
   * @param ModelInterface $model
   *
   * @return self
   */
  public function removeModel(ModelInterface $model): self;

  /**
   * Remove all the models from the collection
   *
   * @return self
   */
  public function removeAll(): self;

  /**
   * Loops over all the Models in this collection, and if the "selected" flag
   * on the model is false, the model is removed from the collection
   *
   * @return self
   */
  public function removeNonSelectedModels(): self;

  /**
   * Loops over all the Models in this collection, and if the "selected" flag
   * on the model is true, the model is removed from the collection
   *
   * @return self
   */
  public function removeSelectedModels(): self;

  /**
   * @return integer
   */
  public function getAmountOfSelectedModels(): int;

  /**
   * @return integer
   */
  public function getAmountOfNotSelectedModels(): int;

  /**
   * @return integer
   */
  public function getAmountOfNewModels(): int;

  /**
   * Sets the selected flag of every model in the collection to TRUE
   *
   * @return self
   */
  public function selectAll(): self;

  /**
   * Sets the selected flag of every model in the collection to FALSE
   *
   * @return self
   */
  public function deselectAll(): self;

  /**
   * Validate all the models in this collection, if a relationshipName was
   * passed get the relationship and validate that
   *
   * @param string $relationshipName
   *
   * @return Validation
   */
  public function validate(string $relationshipName = NULL): Validation;

  /**
   * Clear the provided relationship of every model in this Collection
   *
   * @param string $relationshipName
   *
   * @return self
   */
  public function clear(string $relationshipName): self;

  /**
   * Returns an array with all the IDs of the models in the collection, models
   * without an ID (unsaved models) will not be returned
   *
   * @return array
   */
  public function getIds(): array;

  /**
   * Returns all the ids of a relationship of all the models in this collection
   *
   * @param FieldRelationship $relationship
   *
   * @return array
   */
  public function getFieldIds(FieldRelationship $relationship): array;

  /**
   * Create an Array where the provided fieldName is the key, and the value
   * will be the Model, this only works for fields with a unique value In case
   * multiple models exist with the ame field value, only the last item in the
   * collection with that value will be in the result array
   *
   * @param string $fieldName
   *
   * @return array
   */
  public function buildArrayByFieldName(string $fieldName): array;

  /**
   * Returns an array with all the Entities in the Collection
   *
   * @return array
   */
  public function getEntities(): array;

  /**
   * Put a Model or Collection in this Collection
   *
   * @param ModelInterface|CollectionInterface $objectToPut
   *
   * @return self
   */
  public function put(ModelInterface|CollectionInterface $objectToPut): self;

  /**
   * Put a Model or Collection in this Collection's originalModels
   *
   * @param ModelInterface|CollectionInterface $objectToPut
   *
   * @return self
   */
  public function putOriginal(ModelInterface|CollectionInterface $objectToPut): self;

  /**
   * Create a new Model with the same type as this Collection, put it in the
   * Collection and return it
   *
   * @return ModelInterface
   */
  public function putNew(): ModelInterface;

  /**
   * Returns true if this Collection is empty
   *
   * @return boolean
   */
  public function isEmpty(): bool;

  /**
   * Check whether the key of a Model exists in this Collection
   *
   * @param string $key
   *
   * @return boolean
   */
  public function containsKey(string $key): bool;

  /**
   * Check whether a Modle is part of the collection
   *
   * @param ModelInterface $model
   *
   * @return boolean
   */
  public function containsModel(ModelInterface $model): bool;

  /**
   * Get a Model by its identifier from this Collection, returns NULL if the
   * key cannot be found
   *
   * @param string $key
   *
   * @return ModelInterface|null
   */
  public function getModel(string $key): ?ModelInterface;

  /**
   * Refreshes every model in the collection
   *
   * @return self
   */
  public function refresh(): self;

  /**
   * Checks if the modeltype of the collection has the provided relationship
   *
   * @param string $relationshipName
   *
   * @return boolean
   */
  public function hasRelationship(string $relationshipName): bool;

  public function filter(callable $function): static;
}
