<?php

namespace Drupal\spectrum\Model;

use DateTime;
use DateTimeZone;
use Drupal;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\spectrum\Exceptions\InvalidFieldException;
use Drupal\spectrum\Models\File;
use Drupal\spectrum\Models\Image;
use Drupal\spectrum\Serializer\ModelSerializerInterface;
use Drupal\spectrum\Services\ModelServiceInterface;
use Drupal\spectrum\Utils\AddressUtils;
use Psr\Log\LoggerInterface;

/**
 * This class exposes magic getters to get values from a model without having to know the drupal implementation
 * Useful for within Email templates for example, where we can just get {{ account.name }} instead of {{ account.entity.title.value }}
 */
class SimpleModelWrapper
{
  private ModelSerializerInterface $modelSerializer;
  private LoggerInterface $logger;

  /**
   * The wrapped Model
   */
  private ModelInterface $model;

  /**
   * @param Model $model The model you want to wrap
   */
  public function __construct(ModelInterface $model)
  {
    $this->model = $model;
    $this->logger = Drupal::service('logger.channel.spectrum');
    $this->modelSerializer = Drupal::service('spectrum.model_serializer');
  }

  /**
   * Returns the wrapped Model
   *
   * @return ModelInterface
   */
  public function getModel(): ModelInterface
  {
    return $this->model;
  }

  /**
   * Get a value from the model
   *
   * @param string $underscoredField This should be the underscored field name, as twig templates cant handle dashes. use first_name instead of first-name
   * @return mixed
   */
  public function getValue(string $underscoredField)
  {
    /** @var Model $model */
    $model = $this->model;
    $modelClass = $model->getModelClass();

    // email templates can't handle dashes, so we replaced them with underscores
    $prettyField = $this->modelSerializer->getPrettyFieldForUnderscoredField($modelClass, $underscoredField);
    $prettyFieldsToFields = $this->modelSerializer->getPrettyFieldsToFieldsMapping($modelClass);

    if (array_key_exists($prettyField, $prettyFieldsToFields)) {
      /** @var FieldableEntityInterface $entity */
      $entity = $model->getEntity();
      $fieldName = $prettyFieldsToFields[$prettyField];
      $fieldDefinition = $entity->getFieldDefinition($fieldName);
      $fieldCardinality = $fieldDefinition->getFieldStorageDefinition()->getCardinality();

      $fieldType = $fieldDefinition->getType();
      $returnValue = '';

      // TODO: add support for field cardinality
      switch ($fieldType) {
        case 'autonumber':
          $fieldValue = $model->entity->{$fieldName}->value;
          $returnValue = isset($fieldValue) ? (int) $fieldValue : null;

          break;
        case 'boolean':
          $returnValue = ($model->entity->{$fieldName}->value === '1');
          break;
        case 'color_field_type':
          $returnValue = $model->entity->{$fieldName}->color;
          break;
        case 'decimal':
          $fieldValue = $model->entity->{$fieldName}->value;
          $returnValue = isset($fieldValue) ? (float) $fieldValue : null;
          break;
        case 'geolocation':
          $lat = (float) $model->entity->{$fieldName}->lat;
          $lng = (float) $model->entity->{$fieldName}->lng;

          $returnValue = $lat . ',' . $lng;
          break;
        case 'entity_reference':
        case 'entity_reference_revisions':
          $fieldObjectSettings = $fieldDefinition->getSettings();
          if (!empty($fieldObjectSettings) && array_key_exists('target_type', $fieldObjectSettings) && $fieldObjectSettings['target_type'] === 'currency') {
            $returnValue = $model->entity->{$fieldName}->target_id;
          }

          break;
        case 'image':
          $fileId = $model->entity->{$fieldName}->target_id;

          if (!empty($fileId)) {
            $returnValue = Image::forgeById($fileId);
          }
          break;
        case 'file':
          $fileId = $model->entity->{$fieldName}->target_id;

          if (!empty($fileId)) {
            $returnValue = File::forgeById($fileId);
            // TODO File/Image model fix
            $returnValue = new File($returnValue->entity);
          }
          break;
        case 'link':
          $returnValue = $model->entity->{$fieldName}->uri;
          break;
        case 'address':
          $value = $model->entity->{$fieldName};
          $returnValue = AddressUtils::getAddress($value);
          break;
        case 'created':
        case 'changed':
        case 'timestamp':
          // For some reason, created and changed aren't regular datetimes, they
          // are unix timestamps in the database.
          $timestamp = $model->entity->{$fieldName}->value;
          $returnValue = DateTime::createFromFormat('U', $timestamp);
          break;
        case 'datetime':
          $dateValue = null;
          $attributeValue = $model->entity->{$fieldName}->value;

          if (!empty($attributeValue)) {
            // We must figure out if this is a Date field or a datetime field
            // lets get the meta information of the field
            $fieldSettingsDatetimeType = $fieldDefinition
              ->getItemDefinition()
              ->getSettings()['datetime_type'];
            if ($fieldSettingsDatetimeType === 'date') {
              $dateValue = new DateTime($attributeValue);
            } else if ($fieldSettingsDatetimeType === 'datetime') {
              $dateValue = new DateTime(
                $attributeValue,
                new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE)
              );
            }
          }

          $returnValue = $dateValue;
          break;
        case 'uri':
        default:
          if ($fieldCardinality !== 1) {
            // More than 1 value allowed in the field
            $value = [];
            $fieldValues = $model->entity->{$fieldName};
            foreach ($fieldValues as $fieldValue) {
              $value[] = $fieldValue->value;
            }
          } else {
            $value = $model->entity->{$fieldName}->value;
          }

          $returnValue = $value;
          break;
      }

      return $returnValue;
    }

    throw new InvalidFieldException();
  }

  /**
   * Magic getters that facilitate the use in twig templates
   *
   * @param string $property
   * @return mixed
   */
  public function __get($property)
  {
    /** @var Model $model */
    $model = $this->model;
    $modelClass = $model->getModelClass();

    if ($model->isRelatedViaFieldRelationshipInMemory($property)) // lets check for pseudo properties
    {
      $object = $model->get($property);

      if ($object instanceof ModelInterface) {
        return new SimpleModelWrapper($object);
      }

      if ($object instanceof Collection) {
        return new SimpleCollectionWrapper($object);
      }

      return null;
    } elseif ($model->isRelatedViaReferencedRelationshipInMemory($property)) // lets check for pseudo properties
    {
      $object = $model->get($property);

      if ($object instanceof ModelInterface) {
        return new SimpleModelWrapper($object);
      }

      if ($object instanceof Collection) {
        return new SimpleCollectionWrapper($object);
      }

      return NULL;
    } else if ($this->modelSerializer->underScoredFieldExists($modelClass, $property)) {
      try {
        $value = $this->getValue($property);
        return $value;
      } catch (InvalidFieldException $exception) {
        $this->logger->error('Property ' . $property . ' does not exist on modelclass ' . get_called_class());
      }
    } else if (property_exists($model, $property)) {
      $returnValue = $model->{$property};
      return $returnValue;
    } else if (Model::getterExists($model, $property)) {
      return $model->callGetter($property);
    } else if ($property === 'typeKey') {
      return Drupal::service('spectrum.model')->getBundleKey($modelClass);
    }
  }

  /**
   * Needed for twig to be able to access relationship via magic getter
   *
   * @param string $property
   * @return boolean
   */
  public function __isset($property)
  {
    /** @var Model $model */
    $model = $this->model;
    $modelClass = $model->getModelClass();

    $isSet = $model->isRelatedViaFieldRelationshipInMemory($property)
      || $model->isRelatedViaReferencedRelationshipInMemory($property)
      || $this->modelSerializer->underScoredFieldExists($modelClass, $property)
      || property_exists($model, $property)
      || Model::getterExists($model, $property)
      || $property === 'typeKey';
    return $isSet;
  }
}
