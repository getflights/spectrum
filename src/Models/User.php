<?php

namespace Drupal\spectrum\Models;

use Drupal\Core\Entity\EntityInterface;
use Drupal\spectrum\Model\Model;
use Drupal\spectrum\Permissions\AccessPolicy\AccessPolicyInterface;
use Drupal\spectrum\Permissions\AccessPolicy\PublicAccessPolicy;
use Drupal\spectrum\Services\PermissionServiceInterface;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User as DrupalUser;

/**
 * Class User
 *
 * @package Drupal\spectrum\Models
 */
class User extends Model
{
  private PermissionServiceInterface $permissions;

  public function __construct(
    EntityInterface $entity
  ) {
    parent::__construct($entity);
    $this->permissions = \Drupal::service('spectrum.permissions');
  }

  /**
   * The Entitytype of this model
   *
   * @return string
   */
  public static function entityType(): string
  {
    return 'user';
  }

  /**
   * The Bundle of this model
   *
   * @return string
   */
  public static function bundle(): string
  {
    return '';
  }

  /**
   * The relationships to other models.
   *
   * @return void
   */
  public static function relationships()
  {
    parent::relationships();
  }

  public function getId() {
    /** @var \Drupal\user\Entity\User $entity */
    $entity = $this->entity;
    return $entity->isAnonymous() ? 0 : $entity->id();
  }

  /**
   * @inheritDoc
   */
  public function afterInsert()
  {
    parent::afterInsert();
    $this->permissions->rebuildAccessPoliciesForUser($this);
  }

  public function shouldRebuildAccessPolicy(): bool
  {
    return TRUE;
  }
  /**
   * @inheritDoc
   */
  public function afterUpdate()
  {
    parent::afterUpdate();
    if($this->shouldRebuildAccessPolicy()){
      $this->permissions->rebuildAccessPoliciesForUser($this);
    }
  }

  /**
   * @inheritDoc
   */
  public function beforeDelete()
  {
    parent::beforeDelete();
    $this->permissions->removeUserFromAccessPolicies($this);
  }

  /**
   * @inheritDoc
   */
  public static function getAccessPolicy(): AccessPolicyInterface
  {
    return new PublicAccessPolicy;
  }

  /**
   * Returns the roles of the user
   *
   * @return string[]
   */
  public function getRoles(): array
  {
    /** @var DrupalUser $entity */
    $entity = $this->entity;
    return $entity->getRoles();
  }

  /**
   * Check if the user is an Anonymous user
   *
   * @return boolean
   */
  public function isAnonymous(): bool
  {
    /** @var DrupalUser $entity */
    $entity = $this->entity;
    return $entity->isAnonymous();
  }

  /**
   * Check if a role exist on the User
   *
   * @param string $role
   * @return boolean
   */
  public function hasRole(string $role): bool
  {
    $roles = $this->getRoles();
    return in_array($role, $roles);
  }

  public function addRole(Role $role): User
  {
    /** @var DrupalUser $entity */
    $entity = $this->entity;
    $entity->addRole($role->id());

    return $this;
  }

  public function removeRole(Role $role): User
  {
    /** @var DrupalUser $entity */
    $entity = $this->entity;
    $entity->removeRole($role->id());

    return $this;
  }

  /**
   * @return DrupalUser
   */
  public function getDrupalEntity(): DrupalUser
  {
    return $this->getEntity();
  }

  /**
   * Check if the user is active
   *
   * @return boolean
   */
  public function isActive(): bool
  {
    /** @var DrupalUser $entity */
    $entity = $this->entity;
    return $entity->isActive();
  }

  /**
   * Returns true if the current user has the role Administrator
   *
   * @return boolean
   */
  public function isAdministrator(): bool
  {
    return $this->hasRole('administrator');
  }

  /**
   * Activate the user (in memory, model must be saved to persist)
   *
   * @return User
   */
  public function activate(): User
  {
    $this->entity->{'status'}->value = TRUE;
    return $this;
  }

  /**
   * Block the user (in memory, model must be saved to persist)
   *
   * @return User
   */
  public function block(): User
  {
    $this->entity->{'status'}->value = FALSE;
    return $this;
  }

  /**
   * Sets the Unhashed password on the User
   *
   * @param string $value
   * @return User
   */
  public function setPassword(string $value): User
  {
    /** @var DrupalUser $entity */
    $entity = $this->entity;

    $entity->setPassword($value);
    return $this;
  }

  /**
   * @param string $value
   *
   * @return User
   */
  public function setEmail(string $value): User
  {
    $this->entity->{'mail'}->value = $value;
    return $this;
  }

  /**
   * @return string
   */
  public function getEmail(): string
  {
    return $this->entity->{'mail'}->value;
  }

  /**
   * @param string $value
   *
   * @return User
   */
  public function setName(string $value): User
  {
    $this->entity->{'name'}->value = $value;
    return $this;
  }

  /**
   * @return string
   */
  public function getUsername(): string
  {
    if ($this->getId() == 0) {
      return 'Anonymous';
    }

    return $this->entity->{'name'}->value;
  }
}
