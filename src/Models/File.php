<?php

namespace Drupal\spectrum\Models;

use DateTime;
use Drupal\spectrum\Model\Model;
use Drupal\spectrum\Permissions\AccessPolicy\AccessPolicyInterface;
use Drupal\spectrum\Permissions\AccessPolicy\PublicAccessPolicy;
use Drupal\spectrum\Utils\UrlUtils;
use Drupal\file\Entity\File as FileEntity;

/**
 * A File model for the file entity
 */
class File extends Model
{
  /**
   * The Entitytype of this model
   *
   * @return string
   */
  public static function entityType(): string
  {
    return 'file';
  }

  /**
   * The Bundle of this model
   *
   * @return string
   */
  public static function bundle(): string
  {
    return '';
  }

  /**
   * The relationships to other models
   *
   * @return void
   */
  public static function relationships()
  {
    parent::relationships();
  }

  /**
   * @inheritDoc
   */
  public static function getAccessPolicy(): AccessPolicyInterface
  {
    return new PublicAccessPolicy;
  }

  /**
   * This function can be used in dynamic api handlers
   *
   * @return string
   */
  protected function getBaseApiPath(): string
  {
    return 'file';
  }

  /**
   * Returns an array of the references where this File is being used. These are fields that contain the file
   *
   * @return array
   */
  public function getReferences(): array
  {
    return file_get_file_references($this->entity);
  }

  /**
   * Returns an array of file usage in the file_usage table
   *
   * @return array
   */
  public function getUsage(): array
  {
    $fileUsageService = \Drupal::service('file.usage');
    $usage = $fileUsageService->listUsage($this->entity);

    return empty($usage) ? [] : $usage;
  }

  /**
   * Returns true if this file is referenced in other entities
   *
   * @return boolean
   */
  public function hasReferences(): bool
  {
    return sizeof($this->getReferences()) > 0;
  }

  /**
   * Returns true if the file exists in the file_usage table
   *
   * @return boolean
   */
  public function hasUsage(): bool
  {
    return sizeof($this->getUsage()) > 0;
  }

  /**
   * Returns true if the file has references or is listed in the file_usage table
   *
   * @return boolean
   */
  public function isInUse(): bool
  {
    return $this->hasReferences() || $this->hasUsage();
  }

  /**
   * Removes this file from all the references. So if an entity in the system refers to this file. Set that reference to NULL
   *
   * @return void
   */
  public function removeFromReferences()
  {
    $references = $this->getReferences();

    // We loop over the references
    foreach ($references as $fieldName => $referencedEntityTypes) {
      // Next we loop over every entitytype containting the different entities
      foreach ($referencedEntityTypes as $entityType => $referencedEntities) {
        // Next we loop over every entity containing the reference
        foreach ($referencedEntities as $entityId => $entity) {
          $entity->$fieldName->target_id = null;
          $entity->save();
        }
      }
    }
  }

  /**
   * Returns a hash based on the UUID, with this hash you can validate requests for files, and see if it matches the FID in the database
   *
   * @return string
   */
  public function getHash(): string
  {
    return md5($this->entity->{'uuid'}->value);
  }

  /**
   * Return the real SRC of the file, this will return a direct link to the file, specific for the file back-end storage
   *
   * @return string
   */
  public function getRealSrc(): string
  {
    /** @var FileEntity $entity */
    $entity = $this->entity;
    return $entity->url();
  }

  public function getFilename(): string
  {
    /** @var FileEntity $entity */
    $entity = $this->entity;
    return $entity->getFilename();
  }

  public function getFileUri(): string
  {
    /** @var FileEntity $entity */
    $entity = $this->entity;
    return $entity->getFileUri();
  }

  public function getMimeType(): string
  {
    /** @var FileEntity $entity */
    $entity = $this->entity;
    return $entity->getMimeType();
  }

  public function getSize(): int
  {
    /** @var FileEntity $entity */
    $entity = $this->entity;
    return $entity->getSize();
  }

  public function getCreatedTime(): DateTime
  {
    /** @var FileEntity $entity */
    $entity = $this->entity;
    return DateTime::createFromFormat('U', $entity->getCreatedTime());
  }

  /**
   * @return string
   */
  public function getUri(): string
  {
    return $this->entity->{'uri'}->value;
  }

  /**
   * Returns a base64 encoded string of the file
   *
   * @return string
   */
  public function getBase64SRC(): string
  {
    $mime = $this->entity->{'filemime'}->value;
    $base64 = base64_encode(file_get_contents($this->getUri()));

    return 'data:' . $mime . ';base64,' . $base64;
  }

  /**
   * Return a Drupal absolute URL that you can use to return the File indepentenly of the File storage back-end
   * All the information to get the file is contained in the URL. the FID (file ID), and DG an MD5 hash of the UUID (so you can validate the call by an extra param)
   *
   * @return string
   */
  public function getSRC(): string
  {
    $url = UrlUtils::getBaseURL() . $this->getBaseApiPath() . '/' . urlencode($this->entity->{'filename'}->value) . '?fid=' . $this->getId() . '&dg=' . $this->getHash();

    return $url;
  }
}
