<?php

namespace Drupal\spectrum\Query;

use Drupal;
use Drupal\spectrum\Data\ChunkedIterator;
use Drupal\spectrum\Model\Model;
use Drupal\spectrum\Model\PolymorphicCollection;
use Generator;

/**
 * Class EntityQuery
 *
 * An EntityQuery, is the most basic query around, it provides a way of
 * querying results with a certain entity. (multiple bundles can be returned)
 *
 * @package Drupal\spectrum\Query
 */
class EntityQuery extends Query {

  /**
   * Execute the query, and fetch a single Model, if multiple entities are
   * found, the first one is returned. If nothing is found, null is returend
   * @template T
   *
   * @param class-string<T> $class
   *
   * @return T|\Drupal\spectrum\Model\Model|null
   */
  public function fetchSingleModel(string $class = NULL): ?Model {
    $entity = $this->fetchSingle();
    return isset($entity) ? Model::forgeByEntity($entity, $class) : NULL;
  }

  /**
   * @return \Generator
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function fetchGenerator(): Generator {
    $storage = Drupal::entityTypeManager()->getStorage($this->entityType);
    $entities = new ChunkedIterator($storage, $this->fetchIds());

    foreach ($entities as $entity) {
      yield $entity;
    }
  }

  public function fetchChunkedGenerator(int $chunkSize = 50): Generator {
    $chunkedIterator = new ChunkedIterator(
      Drupal::entityTypeManager()->getStorage($this->entityType),
      $this->fetchIds(),
      $chunkSize
    );

    yield from $chunkedIterator->getChunkedIterator();
  }

  public function fetchModelGenerator(): Generator {
    /** @var \Drupal\spectrum\Services\ModelServiceInterface $modelService */
    $modelService = Drupal::service('spectrum.model');

    foreach ($this->fetchGenerator() as $entity) {
      if ($entity instanceof Drupal\Core\Entity\EntityInterface) {
        /** @var Model $class */
        $class = $modelService->getModelClassForEntity($entity);
        yield $class::forgeByEntity($entity);
      } else {
        yield $entity;
      }
    }
  }

  public function fetchCollection(): Drupal\spectrum\Model\CollectionInterface {
    $models = iterator_to_array($this->fetchModelGenerator());
    return PolymorphicCollection::forgeByModels(NULL, $models);
  }

}
