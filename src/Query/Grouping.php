<?php

namespace Drupal\spectrum\Query;

/**
 * This class is used to add Grouping to your query.
 */
class Grouping
{
  public function __construct(
    protected string $fieldName,
    protected ?string $alias = null
  ) {
  }

  /**
   * Get the Drupal field name
   *
   * @return  string
   */
  public function getFieldName(): string
  {
    return $this->fieldName;
  }

  /**
   * @param string $fieldName
   * @return self
   */
  public function setFieldName(string $fieldName): self
  {
    $this->fieldName = $fieldName;
    return $this;
  }

  /**
   * Get the sorting alias
   *
   * @return string|null
   */
  public function getAlias(): ?string
  {
    return $this->alias;
  }

  /**
   * @param string|null $alias
   * @return self
   */
  public function setAlias(?string $alias): self
  {
    $this->alias = $alias;
    return $this;
  }
}
