<?php

namespace Drupal\spectrum\Query;

use Drupal\Core\Database\Query\Select;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Query\Condition as DrupalCondition;

/**
 * This class provides base functionality for different query types
 */
class AggregateQuery extends QueryBase
{
  protected const GROUPING_PLACEHOLDER = '__pseudo_grouping_placeholder_';

  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Here the Aggregations are stored
   *
   * @var Aggregation[]
   */
  protected array $aggregations = [];

  private bool $expressionInGroupings = false;

  /**
   * All the fields where groupings were added
   *
   * @var Grouping[]
   */
  private array $groupings = [];

  public function __construct(string $entityType)
  {
    parent::__construct($entityType);
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
  }

  /**
   * @param Aggregation $aggregation
   * @return self
   */
  public function addAggregation(Aggregation $aggregation): self
  {
    $this->aggregations[] = $aggregation;
    return $this;
  }

  /**
   * @return array
   */
  public function getAggregations(): array
  {
    return $this->aggregations;
  }

  /**
   * @return $this
   */
  public function clearAggregations(): self
  {
    $this->aggregations = [];
    return $this;
  }

  /**
   * Define a range for the results. this will override any limit that was previously set
   *
   * @param integer $start
   * @param integer $length
   * @return self
   */
  public function setRange(int $start, int $length): self
  {
    $this->rangeStart = $start;
    $this->rangeLength = $length;
    return $this;
  }

  /**
   * Return a DrupalQuery with all the conditions and other configurations applied to the query
   *
   * @return QueryAggregateInterface
   */
  public function getQuery(): QueryAggregateInterface
  {
    $query = $this->getBaseQuery();

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected final function getDrupalQuery(): QueryInterface
  {
    return $this->entityTypeManager->getStorage($this->entityType)->getAggregateQuery();
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseQuery(): QueryAggregateInterface
  {
    /** @var QueryAggregateInterface $query */
    $query = parent::getBaseQuery();
    $query->accessCheck(TRUE);

    // Next the aggregations
    /** @var Aggregation $aggregation */
    foreach ($this->getAggregations() as $aggregation) {
      if (!$this->hasExpression($aggregation->getFieldName())) {
        $alias = $aggregation->getAlias();
        $query->aggregate($aggregation->getFieldName(), $aggregation->getAggregateFunction(), $aggregation->getLangcode(), $alias);
      }
    }

    if (!$query->hasTag('spectrum_query')) {
      $query->addTag('spectrum_query')->addMetaData('spectrum_query', $this);
    }

    $this->prepareGroupings($query);

    return $query;
  }

  /**
   * @param QueryAggregateInterface $drupalQuery
   * @return self
   */
  private function prepareGroupings(QueryAggregateInterface $drupalQuery): self
  {
    // Next the groupings
    foreach ($this->getGroupings() as $grouping) {
      if ($this->hasExpression($grouping->getFieldName()) || !empty($grouping->getAlias())) {
        $this->expressionInGroupings = true;
        break;
      }
    }

    if (!$this->expressionInGroupings) {
      // No expressions, lets use the default drupal groupBy to keep things simple

      foreach ($this->getGroupings() as $grouping) {
        $drupalQuery->groupBy($grouping->getFieldName());
      }
    } else {
      // Some more haxory ^___^
      // Because the order of the groupings is important, we cannot simply ignore the groupings with expressions
      // and add them in the alter hook later on. All the groupings must be added in order.
      // Because we can have a mix of groupings with expressions and without, this becomes tricky
      // To achieve this, we do not add any grouping in case an expression is found, and add them later.
      // however because it might be that a groupBy is done on a parent field (eg field_company.entity.field_company_type)
      // we must force a JOIN on that table.
      // Just like in the normal Query, we achieve this by adding a pseudo condition, which we will remove in the alter hook

      $pseudoGroupingConditionGroup = new ConditionGroup();

      $logic = [];
      foreach ($this->getGroupings() as $index => $grouping) {
        if (!$this->hasExpression($grouping->getFieldName())) {
          // A grouping without an expression, lets force the join
          // no need to do this for the expression, it is done through the expression logic already

          $logicIndex = sizeof($logic);
          $pseudoGroupingConditionGroup->addCondition(new Condition($grouping->getFieldName(), 'LIKE', self::GROUPING_PLACEHOLDER . $index));
          $logic[] = $logicIndex + 1;
        }
      }


      if ($pseudoGroupingConditionGroup->hasConditions()) {
        $pseudoGroupingConditionGroup->setLogic('OR(' . implode(',', $logic) . ')');
        $pseudoGroupingConditionGroup->applyConditionsOnQuery($drupalQuery);
      }
    }
    return $this;
  }

  /**
   * Execute the query and return the results
   *
   * @return array
   */
  public function fetchResults(): array
  {
    $query = $this->getQuery();
    $result = $query->execute();

    return empty($result) ? [] : $result;
  }

  /**
   * @return Grouping[]
   */
  public function getGroupings(): array
  {
    return $this->groupings;
  }

  /**
   * @param Grouping $grouping
   * @return self
   */
  public function addGrouping(Grouping $grouping): self
  {
    $this->groupings[] = $grouping;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function parseExpressions(Select $drupalQuery): self
  {
    parent::parseExpressions($drupalQuery);

    if ($this->expressionInGroupings) {
      // Here we will remove the pseudo grouping condition that was added earlier
      // But we must also get to know what actual SQL column to group on,
      // we can know this by looking at the index that was added during the setup
      // this way we have a way of mapping the n-th grouping to a column in the query

      $groupingIndexMapping = [];
      $pseudoConditionGroupKey = null;

      // First we find the column mapping from the drupal query and the key to unset
      foreach ($drupalQuery->conditions() as $key => $condition) {
        if (is_array($condition) && $condition['field'] instanceof DrupalCondition) {
          /** @var DrupalCondition $conditionGroup */
          $conditionGroup = $condition['field'];

          foreach ($conditionGroup->conditions() as $subCondition) {
            if (is_array($subCondition) && isset($subCondition['value']) && is_string($subCondition['value']) && substr($subCondition['value'], 0, strlen(self::GROUPING_PLACEHOLDER)) === self::GROUPING_PLACEHOLDER) {
              $groupingIndex = (int) ltrim($subCondition['value'], self::GROUPING_PLACEHOLDER);
              $groupingIndexMapping[$groupingIndex] = $subCondition['field'];
              $pseudoConditionGroupKey = $key;
            }
          }
        }
      }

      // We unset the pseudo conditiongroup, it cannot be added else the query will not work
      // the only reason was to force drupal to include the JOINS
      unset($drupalQuery->conditions()[$pseudoConditionGroupKey]);

      // Next we add groupBy's to the query
      // in case the grouping has an expression, we can simply add the groupBy (the expression was added earlier)
      // and expressions are added to the fields automatically
      // however, simple groupBy's must also be added to the fields, in order to know what the values are we are grouping on
      foreach ($this->groupings as $index => $grouping) {
        if ($this->hasExpression($grouping->getAlias())) {
          $drupalQuery->groupBy($grouping->getAlias());
        } else {
          $drupalQuery->groupBy($groupingIndexMapping[$index]);

          [$table, $field] = explode('.', $groupingIndexMapping[$index]);
          $drupalQuery->addField($table, $field, $grouping->getAlias() ?? self::GROUPING_PLACEHOLDER . $index);
        }
      }
    }

    return $this;
  }
}
