<?php

declare(strict_types=1);

namespace Drupal\spectrum\Event;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigImporterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class EventSubscriber implements EventSubscriberInterface {

  public function __construct(
    private readonly CacheBackendInterface $cache,
  ) {}

  public static function getSubscribedEvents() {
    $events[ConfigEvents::IMPORT][] = ['onChange'];

    return $events;
  }

  public function onChange(ConfigImporterEvent $event) {
    $this->cache->deleteAll();
  }

}