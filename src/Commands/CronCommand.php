<?php

declare(strict_types=1);

namespace Drupal\spectrum\Commands;

use DateTime;
use DateTimeZone;
use Drupal;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\State\StateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\spectrum\Query\Condition;
use Drupal\spectrum\Query\ConditionGroup;
use Drupal\spectrum\Query\Order;
use Drupal\spectrum\Runnable\QueuedJob;
use Drupal\spectrum\Runnable\RegisteredJob;
use Drupal\spectrum\Services\ModelServiceInterface;
use Drupal\spectrum\Services\ModelStoreInterface;
use Drush\Commands\DrushCommands;
use React\EventLoop\Loop;
use Throwable;

/**
 * Class CronCommand.
 */
final class CronCommand extends DrushCommands {

  public function __construct(
    private readonly MemoryCacheInterface $memoryCache,
    private readonly StateInterface $stateCache,
    private readonly ModelStoreInterface $modelStore,
    private readonly ModelServiceInterface $modelService
  ) {
    parent::__construct();
  }

  /**
   * @command spectrum:cron
   * @aliases sp:cron
   *
   * @return void
   */
  public function execute(string $executor = 'default'): void {
    $registeredJobIds = $this->getRegisteredJobIds($executor);

    $this->say('Spectrum Cron Started');

    $locked = FALSE;

    $output = $this->output();
    Loop::addPeriodicTimer(1, function() use ($output, &$locked, $registeredJobIds) {
      if ($locked) {
        return;
      }

      $queuedJob = $this->getNextQueuedJob($registeredJobIds);

      if ($queuedJob === NULL) {
        return;
      }

      $this->clearAllCaches();
      $queuedJob->setOutput($output);
      /** @var RegisteredJob $job */
      $job = $queuedJob->fetch('job');

      if ($job === NULL) {
        $queuedJob->failedExecution(NULL, 'Registered Job no longer exists');
        $this->clearAllCaches();
        return;
      }

      $class = $job->getJobClass();
      if ($class === NULL) {
        $queuedJob->failedExecution(NULL, 'No class provided');
        $this->clearAllCaches();
        return;
      }

      if (!class_exists($class)) {
        $queuedJob->failedExecution(NULL, 'Class does not exist');
        $this->clearAllCaches();
        return;
      }

      if (!$job->isActive()) {
        $queuedJob->failedExecution(NULL, 'Registered job is inactive');
        $this->clearAllCaches();
        return;
      }

      $locked = TRUE;

      try {
        /** @var QueuedJob $job */
        $job = $class::forgeByEntity($queuedJob->entity);
        $job->setOutput($output);

        $job->run();
      }
      catch (Throwable) {
        // Handled in job.
      }

      $locked = FALSE;

      $this->clearAllCaches();
    });

    Loop::run();
  }

  private function getNextQueuedJob(array $registeredJobIds): ?QueuedJob {
    // @todo: inject clock
    $currentTime = new DateTime(
      'now',
      new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE)
    );
    $currentTime = $currentTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $lock = (new ConditionGroup())
      ->addCondition(new Condition(
        'field_lock',
        '<',
        $currentTime
      ))
      ->addCondition(new Condition(
        'field_lock',
        '=',
        'null'
      ))
      ->setLogic('OR(1,2)');

    $queuedJob = QueuedJob::getModelQuery()
      ->addCondition(new Condition(
        'field_job_status',
        'IN',
        [QueuedJob::STATUS_QUEUED, QueuedJob::STATUS_RUNNING]
      ))
      ->addCondition(new Condition(
        'field_scheduled_time',
        '<=',
        $currentTime
      ))
      ->addCondition(new Condition(
        'field_job.target_id',
        'IN',
        $registeredJobIds
      ))
      ->addConditionGroup($lock)
      ->addSortOrder(new Order('field_scheduled_time'))
      ->setLimit(1)
      ->fetchSingleModel();

    return $queuedJob;
  }

  private function clearAllCaches(): self {
    // This will clear all the entity caches, and free entities from memory
    $this->modelService->clearDrupalEntityCachesForAllModels();

    // Reset some extra Drupal Caches
    $this->memoryCache->deleteAll();
    $this->stateCache->resetCache();

    // And finally clear the model store of any data as well
    $this->modelStore->unloadAll();
    return $this;
  }

  private function getRegisteredJobIds(string $executor): array {
    /** @var \Drupal\spectrum\Services\JobServiceInterface $jobservice */
    $jobservice = Drupal::service('spectrum.jobs');

    $jobservice->rebuildRegisteredJobs();
    $definitions = $jobservice->getDefinitions();
    ksort($definitions);

    $classes = [];

    foreach ($definitions as $definition) {
      if ($executor === ($definition['executor'] ?? 'default')) {
        $classes[] = $definition['class'];
      }
    }

    return RegisteredJob::getModelQuery()
      ->addCondition(new Condition(
        'field_class',
        'IN',
        $classes,
      ))
      ->fetchIds();
  }

}
