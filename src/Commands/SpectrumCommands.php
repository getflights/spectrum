<?php

namespace Drupal\spectrum\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use DateTime;
use DateTimeZone;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\State\StateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\spectrum\Query\Condition;
use Drupal\spectrum\Query\ConditionGroup;
use Drupal\spectrum\Query\Order;
use Drupal\spectrum\Runnable\QueuedJob;
use Drupal\spectrum\Runnable\RegisteredJob;
use Drupal\spectrum\Services\JobService;
use Drupal\spectrum\Services\ModelServiceInterface;
use Drupal\spectrum\Services\ModelStoreInterface;
use Drush\Commands\DrushCommands;
use React\EventLoop\Loop;
use Throwable;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class SpectrumCommands extends DrushCommands {

  public function __construct(
    private MemoryCacheInterface $memoryCache,
    private StateInterface $stateCache,
    private ModelStoreInterface $modelStore,
    private ModelServiceInterface $modelService,
    private JobService $jobService,
    private Connection $db
  )
  {
    parent::__construct();
  }

  /**
   * @command spectrum:cron
   *
   * @return void
   */
  public function cron(){
    $this->say('Spectrum Cron Started');

    $locked = FALSE;
    $output = $this->output();
    Loop::addPeriodicTimer(1, function () use ($output, &$locked) {
      if ($locked) {
        return;
      }

      $queuedJob = $this->getNextQueuedJob();

      if ($queuedJob === NULL) {
        return;
      }

      $this->clearAllCaches();
      $queuedJob->setOutput($output);
      /** @var RegisteredJob $job */
      $job = $queuedJob->fetch('job');

      if ($job === NULL) {
        $queuedJob->failedExecution(NULL, 'Registered Job no longer exists');
        $this->clearAllCaches();
        return;
      }

      $class = $job->getJobClass();
      if ($class === NULL) {
        $queuedJob->failedExecution(NULL, 'No class provided');
        $this->clearAllCaches();
        return;
      }

      if (!class_exists($class)) {
        $queuedJob->failedExecution(NULL, 'Class does not exist');
        $this->clearAllCaches();
        return;
      }

      if (!$job->isActive()) {
        $queuedJob->failedExecution(NULL, 'Registered job is inactive');
        $this->clearAllCaches();
        return;
      }

      $locked = TRUE;

      try {
        /** @var QueuedJob $job */
        $job = $class::forgeByEntity($queuedJob->entity);
        $job->setOutput($output);

        $job->run();
      } catch (Throwable) {
        // Handled in job.
      }

      $locked = FALSE;

      $this->clearAllCaches();
    });

    Loop::run();
  }


  private function getNextQueuedJob(): ?QueuedJob {
    // @todo: inject clock
    $currentTime = new DateTime(
      'now',
      new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE)
    );
    $currentTime = $currentTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $lock = (new ConditionGroup)
      ->addCondition(new Condition(
        'field_lock',
        '<',
        $currentTime
      ))
      ->addCondition(new Condition(
        'field_lock',
        '=',
        'null'
      ))
      ->setLogic('OR(1,2)');

    $queuedJob = QueuedJob::getModelQuery()
      ->addCondition(new Condition(
        'field_job_status',
        'IN',
        [QueuedJob::STATUS_QUEUED, QueuedJob::STATUS_RUNNING]
      ))
      ->addCondition(new Condition(
        'field_scheduled_time',
        '<=',
        $currentTime
      ))
      ->addConditionGroup($lock)
      ->addSortOrder(new Order('field_scheduled_time'))
      ->setLimit(1)
      ->fetchSingleModel();

    return $queuedJob;
  }

  private function clearAllCaches(): self {
    // This will clear all the entity caches, and free entities from memory
    $this->modelService->clearDrupalEntityCachesForAllModels();

    // Reset some extra Drupal Caches
    $this->memoryCache->deleteAll();
    $this->stateCache->resetCache();

    // And finally clear the model store of any data as well
    $this->modelStore->unloadAll();
    return $this;
  }
}
